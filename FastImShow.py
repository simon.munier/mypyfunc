#!/bin python

'''
Fast Plotter for Large Images - Resamples Images to a target resolution on each zoom.
Example::
    sz = (10000,20000) # rows, cols
    buf = np.arange(sz[0]*sz[1]).reshape(sz)
    extent = (100,150,1000,2000)  # arbitrary extent
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    im = FastImshow(buf,ax,extent=extent,tgt_res=1024)
    im.show()
    plt.show()
'''

import numpy as np
import matplotlib.pyplot as plt
#from mpl_toolkits.basemap import Basemap


class FastImshow:
    '''
    Fast plotter for large image buffers
    Example::
        sz = (10000,20000) # rows, cols
        buf = np.arange(sz[0]*sz[1]).reshape(sz)
        extent = (100,150,1000,2000)  # arbitrary extent
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        im = FastImshow(buf,ax,extent=extent)
        im.show()
        plt.show()
    '''
    def __init__(self,buf,ax=None,extent=None,MV=None,**kwargs):
        '''
        [in] img buffer
        [in] extent
        [in] axis to plot on
        '''
        self.buf = buf
        self.sz = self.buf.shape
        self.imshow = None
        if ax is None:
          self.axes = plt.gca()
        else:
          self.axes = ax
        if 'fastim' not in list(self.axes.__dict__.keys()): self.axes.fastim = []
        self.axes.fastim.append(self)
        if self.imshow is None: self.imshow = self.axes.imshow
        self.MV = MV
        if extent is not None:
            self.extent = extent
        else:
            self.extent = [ 0, self.sz[1], 0, self.sz[0] ]
        self.startx = self.extent[0]
        self.starty = self.extent[2]
        self.dx = self.sz[1]*1./(self.extent[1]-self.startx) # extent dx
        self.dy = self.sz[0]*1./(self.extent[3]-self.starty) # extent dy
        self.transform = None
        if 'transform' in kwargs:
            self.transform = kwargs['transform']
        self.show(**kwargs)
    # end __init__

    def remove(self):
        '''
        FastImShow destructor
        '''
        self.im.remove()
    def remove_image(self):
        #self.axes.fastim = [im for im in self.axes.fastim if im!=self]
        self.axes.fastim.remove(self)
        self.im.remove_old()
        self.axes.callbacks.disconnect(self.callbacks['xlim'])
        self.axes.callbacks.disconnect(self.callbacks['ylim'])
        self.axes.callbacks.disconnect(self.callbacks['resize'])
    # end remove

    def get_strides(self,istart=0,iend=-1,jstart=0,jend=-1):
        '''
        Get sampling strides for a given bounding region. If none is provided,
           use the full buffer size
        '''
        if iend==-1: iend = self.sz[0]
        if jend==-1: jend = self.sz[1]
        tgt_res_x = self.axes.get_window_extent().width
        tgt_res_y = self.axes.get_window_extent().height
        stridex = max(int((iend-istart)/tgt_res_x),1)
        stridey = max(int((jend-jstart)/tgt_res_y),1)
        return stridex,stridey
    # end get_strides

    def ax_update(self, ax=None, update_shared_axes=True):
        '''
        Event handler for re-plotting on zoom
        - gets bounds in img extent coordinates
        - converts to buffer coordinates
        - calculates appropriate strides
        - sets new data in the axis
        '''
        if ax is None: ax = self.axes
        if self.im.axes is None: return
        self.axes.set_autoscale_on(False)  # Otherwise, infinite loop
        # Get the range for the new area
        xmin,ymin,xdelta,ydelta = self.axes.viewLim.bounds
        xmax = xmin + xdelta
        ymax = ymin + ydelta
        if self.transform is not None:
            xmin,ymin = self.transform.transform_point(xmin,ymin,self.axes.projection)
            xmax,ymax = self.transform.transform_point(xmax,ymax,self.axes.projection)
        #if xmin<self.extent[0] or xmax>self.extent[1] or ymin<self.extent[2] or ymax>self.extent[3]:
        #    return
        istart = int(self.dx * ( xmin - self.startx ))
        iend   = int(self.dx * ( xmax - self.startx ))
        istart = min(max(istart,0),self.sz[1])
        iend   = min(max(iend  ,0),self.sz[1])
        if self.im.origin == 'upper':
            jstart = max(self.sz[0]-1-int(self.dy * ( ymax - self.starty )),0)
            jend   = self.sz[0]-1-int(self.dy * ( ymin - self.starty ))
        else:
            jstart = int(self.dy * ( ymin - self.starty ))
            jend   = int(self.dy * ( ymax - self.starty ))
        jstart = min(max(jstart,0),self.sz[0])
        jend   = min(max(jend  ,0),self.sz[0])
        # Update the image object with our new data and extent
        stridex,stridey = self.get_strides(istart,iend,jstart,jend)
        iend   = min(istart+int((iend-istart)/stridex)*stridex+1,self.sz[1])
        jend   = min(jstart+int((jend-jstart)/stridey)*stridey+1,self.sz[0])
        extentx = (self.startx+istart/self.dx,self.startx+iend/self.dx)
        if self.im.origin == 'upper':
            extenty = (self.starty+(self.sz[0]-jend)/self.dy,self.starty+(self.sz[0]-jstart)/self.dy)
        else:
            extenty = (self.starty+jstart/self.dy,self.starty+jend/self.dy)
        self.im.set_data(self.masked_buf(self.buf[jstart:jend:stridey,istart:iend:stridex,...]))
        self.im.set_extent(extentx+extenty)
        self.axes.figure.canvas.draw_idle()
        # Handle other shared axes
        if update_shared_axes:
          shared_axes = set(self.axes.get_shared_x_axes().get_siblings(self.axes)+\
                            self.axes.get_shared_y_axes().get_siblings(self.axes))
          shared_axes.remove(self.axes)
          for aax in [ax for ax in shared_axes if 'fastim' in dir(ax)]:
            aax.set_xlim(self.axes.get_xlim(),emit=False)
            aax.set_ylim(self.axes.get_ylim(),emit=False)
            for im in aax.fastim:
              im.ax_update(update_shared_axes=False)
    # end ax_update

    def masked_buf(self,data):
        '''
        Create a masked array from data
        '''
        if len(data.shape)==3:
          if data.shape[2]==4: return data
          else:
            if data.min()>=0 and data.max()<=1:
              data_alpha = 1*np.ones(data.shape[:2]+(4,),'f4')
            else:
              data_alpha = 255*np.ones(data.shape[:2]+(4,),'u1')
            data_alpha[:,:,:3] = data
            return data_alpha
        else:
          mask = np.isnan(data)
          if self.MV is not None: mask = mask+(data==self.MV)
          return np.ma.masked_where(mask,data)
    # end masked_buf

    def set_data(self,data):
        '''
        Change image data
        '''
        self.buf = data
        self.ax_update(self.axes)
    # end set_data

    def show(self,**kwargs):
        '''
        Initial plotter for buffer
        '''
        stridex, stridey = self.get_strides()
        self.im = self.imshow(self.masked_buf(self.buf[::stridex,::stridey,...]),extent=self.extent,**kwargs)
        self.im.fastim = self
        self.im.remove_old = self.im.remove
        self.im.remove = self.remove_image
        self.ax_update(self.axes)
        self.callbacks = {}
        self.callbacks['xlim'] = self.axes.callbacks.connect('xlim_changed', self.ax_update)
        self.callbacks['ylim'] = self.axes.callbacks.connect('ylim_changed', self.ax_update)
        self.callbacks['resize'] = self.axes.figure.canvas.mpl_connect('resize_event', lambda event: self.ax_update(self.axes))
    # end show

# end ImgDisplay

if __name__=="__main__":
    sz = (10000,20000) # rows, cols
    buf = np.arange(sz[0]*sz[1]).reshape(sz)
    extent = (100,150,1000,2000)
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    im = FastImshow(buf,ax,extent=extent,**kwargs)
    im.show()

    plt.show()
