import numpy as np
from netCDF4 import Dataset,num2date
import subprocess

def nc_copy_structure(ncfile_old,ncfile_new,nc_version='NETCDF3_64BIT',nc_close=True,\
                      dimensions=None,variables=None):
  '''
  Copy the whole structure of a netcdf file (dimensions, variables and attributes)
  Variables are only defined (no value attributed)
  '''
  n0 = Dataset(ncfile_old)
  n1 = Dataset(ncfile_new,'w',format=nc_version)
  # Global attributes
  for att in n0.ncattrs():
    n1.setncattr(att,n0.getncattr(att))
  # Dimensions
  if dimensions is None: dimensions = n0.dimensions
  for dim in dimensions:
    if n0.dimensions[dim].isunlimited(): dimsize = None
    else : dimsize = n0.dimensions[dim].__len__()
    n1.createDimension(dim,dimsize)
  # Variables
  if variables is None: variables = n0.variables
  for var in variables:
    v = n0.variables[var]
    n1.createVariable(var,v.dtype,v.dimensions)
    # Attributes
    for att in v.ncattrs():
      n1.variables[var].setncattr(att,v.getncattr(att))
  n0.close()
  if nc_close: n1.close()
  else: return n1


def nc_dump(ncfile):
  '''
  '''
  subprocess.call('ncdump -h '+ncfile,shell=True)
  return


def nc_date(nc):
  '''
  '''
  fclose = False
  if type(nc) is not Dataset: nc,fclose = Dataset(nc),True
  dat = num2date(nc['time'][:],nc['time'].units,only_use_cftime_datetimes=False)
  if fclose: nc.close()
  return dat


def nc_get(nc,vars_to_get):
  fclose = False
  single_var = False
  if type(nc) is not Dataset: nc,fclose = Dataset(nc),True
  if type(vars_to_get) is str: vars_to_get,single_var = [vars_to_get],True
  var_out = ()
  for v in vars_to_get:
    var_out = var_out+(nc.variables[v][:],)
  if fclose: nc.close()
  if single_var: var_out = var_out[0]
  return var_out


def nc_write(filename,dat,lon,lat,var_dict,time_units,**kwargs):
  #NETCDF3_CLASSIC,NETCDF4,NETCDF4_CLASSIC
  with Dataset(filename,'w',**kwargs) as nc:
    nc.createDimension('time',len(dat))
    nc.createDimension('longitude',len(lon))
    nc.createDimension('latitude',len(lat))
    nc.createVariable('time','f4',('time',),fill_value=1e20)
    nc['time'].setncattr('units',time_units)
    nc['time'][:] = dat-dat[0]
    nc.createVariable('longitude','f4',('longitude',))
    nc['longitude'][:] = lon
    nc.createVariable('latitude','f4',('latitude',))
    nc['latitude'][:] = lat
    for k,v in var_dict.items():
      nc.createVariable(k,v.dtype,('time','latitude','longitude'),fill_value=1e20)
      nc[k][:] = np.where(np.isnan(v),1e20,v)
