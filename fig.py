import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import os
import subprocess
import io
from PyQt5.QtGui import QImage, QGuiApplication
from PyQt5.QtWidgets import QPushButton
if mpl.get_backend() == 'TkAgg':
  import gi
  gi.require_version('Gtk', '3.0')
  from gi.repository import Gtk, Gdk


if mpl.get_backend() == 'TkAgg':
  class _ClipboardWindow(Gtk.Window):
    def __init__(self,filename):
        super().__init__(title="Clipboard Example")
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        self.image = Gtk.Image.new_from_file(filename)
        if self.image.get_storage_type() == Gtk.ImageType.PIXBUF:
            self.clipboard.set_image(self.image.get_pixbuf())


class myFigureClass(mpl.figure.Figure):
  '''
  New figure class with new_features
    - get_position: get position of figure window
    - set_position: set position of figure window
    - clipboard_handler: to copy figure to the clipboard
  '''
  def get_position(self):
    '''
    Get position of figure window (x_left,y_top,width,height)
    '''
    if mpl.get_backend() == 'TkAgg':
      r = self.canvas.manager.window.winfo_geometry().replace('x','+').split('+')
      r = tuple(map(int,r))
      r = (r[2],r[3]-27,r[0],r[1]-34)
      return r
    elif mpl.get_backend() == 'Qt5Agg':
      return self.canvas.window().geometry().getRect()

  def set_position(self,r):
    '''
    Set position of figure window (x_left,y_top,width,height)
    '''
    if mpl.get_backend() == 'TkAgg':
      self.canvas.manager.window.geometry("{2}x{3}+{0}+{1}".format(r[0],r[1],r[2],r[3]+34))
    elif mpl.get_backend() == 'Qt5Agg':
      self.canvas.manager.window.setGeometry(*r)

  def clipboard_handler(self,event):
    '''
    Event function to copy figure to clipboard with 'ctrl+c'
    '''
    if event.key == 'ctrl+c':
      if mpl.get_backend() == 'TkAgg':
        if True:
          f = os.environ['HOME']+'/python-copy-clipboard.png'
          self.savefig(f)
          win = _ClipboardWindow(f)
          win.connect("destroy", Gtk.main_quit)
          win.show_all()
          Gtk.main()
          os.remove(f)
        else:
          p = self.get_position()
          self.canvas.toolbar.pack_forget()
          self.set_position([p[0],p[1],p[2],p[3]-34])
          self.canvas.draw()
          subprocess.run(['gnome-screenshot','-wBc'])
          self.canvas.get_tk_widget().pack_forget()
          self.canvas.toolbar.pack(side='bottom',fill='x')
          self.canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
          self.set_position(p)
      elif mpl.get_backend() == 'Qt5Agg':
        buf = io.BytesIO()
        self.savefig(buf)
        app = QGuiApplication([])
        app.clipboard().setImage(QImage.fromData(buf.getvalue()))
        buf.close()

# Replace pyplot figure function to use myFigureClass
_pyplot_figure = plt.figure
def _my_figure(*args, **kwargs):
  # Check if figure exists
  if len(args)>0 and isinstance(args[0],int) and plt.fignum_exists(args[0]):
    return _pyplot_figure(args[0])
  # If not, create new figure
  kwargs['FigureClass'] = myFigureClass
  if 'figsize' not in kwargs: kwargs['figsize'] = (6.4,4.8)
  fig = _pyplot_figure(*args, **kwargs)
  fig.canvas.mpl_connect('key_press_event', fig.clipboard_handler)
  #
  #w,h = fig.get_size_inches()
  #print(fig.number,w,h)
  #dpi = fig.get_dpi()
  #fig.set_position([1281,27,int(w*dpi),int(h*dpi)])
  #
  return fig
plt._pyplot_figure = _pyplot_figure
plt.figure = _my_figure
plt.figure.__doc__ = _pyplot_figure.__doc__


def figPosition(*args,fig=None):
  '''
  Get or set figure position
  '''
  if fig is None: fig = plt.gcf()
  if len(args)==0:
    x,y,w,h = fig.get_position()
    return x,y,w,h
  elif len(args)==4:
    x,y,w,h = args
  else:
    x,y,w,h = args[0]
  fig.set_position((x,y,w,h))



_oldcolorbar = plt.colorbar
def _newcolorbar(*args,**kwargs):
  try:
    cb = _oldcolorbar(*args,**kwargs)
  except:
    ax = kwargs['ax'] if 'ax' in kwargs else plt.gca()
    im = ax.images[np.argmax([im.zorder for im in ax.images])]
    cb = _oldcolorbar(im,*args,**kwargs)
  cb.ax.tag = 'colorbar'
  return cb
plt.colorbar = _newcolorbar
plt.colorbar.__doc__ = _oldcolorbar.__doc__


def format_data(obj,data_format='{:g}'):
  def new_format(data):
    if isinstance(data,str): return data
    elif isinstance(data,np.ma.MaskedArray) and data.mask: return '--'
    else: return (data_format).format(data)
  obj.format_cursor_data = new_format


class link_axes():
  def __init__(self,*args):
    self.figures,self.axes = None,None
    for a in args:
      if not '__iter__' in dir(a): continue
      if isinstance(a[0],plt.Figure): self.figures = a
      if isinstance(a[0],int): self.figures = a
      if isinstance(a[0],plt.Axes): self.axes = a
    #
    # Get figures and axes to link
    def _get_figures():
      figures_available = [manager.canvas.figure
        for manager in mpl._pylab_helpers.Gcf.get_all_fig_managers()]
      if self.axes is not None:
        self.figures = [a.figure for a in self.axes if a.figure in figures_available]
        self.axes = [a for fig in self.figures for a in self.axes if a in fig.get_axes()]
      else:
        if self.figures is not None:
          self.figures = [fig for fig in figures_available if fig.number in self.figures]
        else:
          self.figures = figures_available
        self.axes = [a for fig in self.figures for a in fig.get_axes()]
        self.axes = [a for a in self.axes if not ('tag' in a.__dict__ and a.tag=='colorbar')]
    _get_figures()
    for fig in self.figures: fig.link_axes = self
    if len(self.figures)==0: return
    #
    # Link axes
    for a in self.axes: a.set_adjustable('box')
    for a in self.axes[1:]:
      a.get_shared_x_axes().join(self.axes[0],a)
      a.get_shared_y_axes().join(self.axes[0],a)
      a.set_xlim(self.axes[0].get_xlim())
      a.set_ylim(self.axes[0].get_ylim())
    #
    # Resize all figures and axes
    def _resize(fig_ref):
      fig_ref.show()
      fig_ref.canvas.manager.window.update()
      r = fig_ref.get_position()
      for f in self.figures:
        f.set_position(r)
      if len(self.figures)==len(self.axes):
        ax = [a for a in self.axes if a.figure==fig_ref][0]
        r = ax.get_position()
        for a in self.axes: a.set_position(r)
    _resize(self.figures[0])
    #
    # And provides 'pagedown'/'pageup' function to change focus
    def _change_focus(event):
      _get_figures()
      titles = [fig.canvas.get_window_title() for fig in self.figures]
      cur_fig = self.figures.index(event.canvas.figure)
      if event.key == 'pagedown':
        new_fig = (cur_fig-1)%len(self.figures)
        plt.figure(self.figures[new_fig].number)
        os.system('wmctrl -a "'+titles[new_fig]+'"')
      elif event.key == 'pageup':
        new_fig = (cur_fig+1)%len(self.figures)
        plt.figure(self.figures[new_fig].number)
        os.system('wmctrl -a "'+titles[new_fig]+'"')
      elif event.key == 'r':
        _resize(event.canvas.figure)
    # Update figures callbacks
    for fig in self.figures:
      to_remove = []
      for k,f in fig.canvas.callbacks.callbacks.get('key_press_event').items():
        if 'func' in dir(f) and f.func.__name__=='_change_focus': to_remove.append(k)
      for k in to_remove:
        fig.canvas.callbacks.callbacks.get('key_press_event').pop(k)
      fig.canvas.mpl_connect('key_press_event',_change_focus)


class LineBuilder:
  def __init__(self,ax=None,color='red'):
    self.ax = ax if ax is not None else plt.gca()
    self.color = color
    self.ms = 10
    self.alpha = 0.5
    self.xs = []
    self.ys = []
    self.istart = np.inf
    self.jstart = np.inf
    self.counter = 0
    self.shape_counter = 0
    self.shape = {}
    self.precision = 10
    self.cid_pnt = None
    self.cid_crs = None
    self.cursor = None
    self.pushbutton = QPushButton("Polyline")
    self.ax.figure.canvas.toolbar.addSeparator()
    self.ax.figure.canvas.toolbar.addWidget(self.pushbutton)
    self.pushbutton.clicked.connect(self._switch_linebuilder)
    self.pushbutton.setCheckable(True)
  def _add_point(self,event):
    if event.inaxes!=self.ax: return
    if event.button == 3:
      if self.counter != 0:
        self.xs.pop()
        self.ys.pop()
        self.hp.set_data(self.xs,self.ys)
        self.ax.figure.canvas.draw()
        self.counter = self.counter - 1
      return
    if self.counter == 0:
      self.istart = event.x
      self.jstart = event.y
      self.xs.append(event.xdata)
      self.ys.append(event.ydata)
    if np.abs(event.x-self.istart)<=self.precision and \
      np.abs(event.y-self.jstart)<=self.precision and self.counter != 0:
      self.xs.append(self.xs[0])
      self.ys.append(self.ys[0])
      self.hp.set_data(self.xs,self.ys)
      self.ax.plot(self.xs[0],self.ys[0],'o',ms=self.ms,color='blue',alpha=self.alpha)
      self.ax.figure.canvas.draw()
      self.shape[self.shape_counter] = np.array([self.xs,self.ys]).T
      self.shape_counter = self.shape_counter + 1
      self.xs = []
      self.ys = []
      self.counter = 0
    else:
      if self.counter == 0:
        self.hp, = self.ax.plot(self.xs,self.ys,'o-',ms=self.ms,color=self.color,alpha=self.alpha)
      else:
        self.xs.append(event.xdata)
        self.ys.append(event.ydata)
        self.hp.set_data(self.xs,self.ys)
      self.ax.figure.canvas.draw()
      self.counter = self.counter + 1
  def _switch_linebuilder(self,checked):
    if checked:
      self.cid_pnt = self.ax.figure.canvas.mpl_connect('button_press_event',self._add_point)
      if self.cursor is None:
        self.cursor = Cursor(self.ax,useblit=True,color='grey',linewidth=0.5)
      else:
        self.cursor.active = True
    else:
      self.ax.figure.canvas.mpl_disconnect(self.cid_pnt)
      self.ax.figure.canvas.mpl_disconnect(self.cid_crs)
      self.cursor.active = False

def rectangle(zone,*args,ax=None,**kwargs):
  if ax is None: ax = plt.gca()
  z = np.array(zone)
  h = plt.plot(z[[0,0,1,1,0],0],z[[0,1,1,0,0],1],*args,**kwargs)
  return h

def resize_colorbar(ax,cax,xy='y'):
  pax = ax.get_position().bounds
  pcax = cax.get_position().bounds
  aspect = cax.get_aspect()
  cax.set_aspect('auto')
  if xy=='x':
    cax.set_position((pax[0],pcax[1],pax[2],pcax[3]))
  else:
    cax.set_position((pcax[0],pax[1],pcax[2],pax[3]))
  cax.set_aspect(aspect)



