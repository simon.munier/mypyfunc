import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import colors
from matplotlib.collections import LineCollection
from cartopy import crs as ccrs, feature as cfeature
from cartopy.mpl import geoaxes
import numpy as np
import warnings
from .FastImShow import FastImshow
from .misc import nargout,available_kwargs,res2lonlat,get_IlonIlat,convention
from .fig import format_data
from .basin import trip_param,merit_param,dir2acc,basin_mask,mask2bnd
from .mod_basin import basin as fbasin
from . import __path__ as myPath

warnings.filterwarnings( "ignore", module = "matplotlib\\..*" )
plt.rcParams['keymap.back'].remove('left')
plt.rcParams['keymap.forward'].remove('right')

def pmap(lon=None, lat=None, data=None, MV=np.nan, mask=None, zone=None, ax=None,
         title='', dat=None, vlim=None, cm='jet', norm=None, interpolation='none',
         projection=None, coast=True, river=False, naturalearth=False,
         grid_coord=True, map_on_network=False,
         *args, **kwargs):

  if zone is None and isinstance(projection,str) and projection.lower()=='l2e':
    zone = [[61000,2706000],[1029000,1500000]]
  elif isinstance(zone,str):
    zone = def_zone(zone)
  if lon is None: lon,res_lon = [-180,180] if zone is None else [zone[0][0],zone[1][0]],0
  else          : res_lon = lon[1]-lon[0]
  if lat is None: lat,res_lat = [90,-90] if zone is None else [zone[0][1],zone[1][1]],0
  else          : res_lat = np.abs(lat[1]-lat[0])
  lon,lat = np.array(lon),np.array(lat)
  if zone is None:
    zone = [[np.min(lon)-res_lon/2.,np.max(lat)+res_lat/2.],
            [np.max(lon)+res_lon/2.,np.min(lat)-res_lat/2.]]
  zone = np.array(zone)
  if projection is None:
    zone[[0,1],[1,0]] = np.minimum(zone[[0,1],[1,0]],[90,180])
    zone[[1,0],[1,0]] = np.maximum(zone[[1,0],[1,0]],[-90,-180])

  if data is None:
    if not map_on_network: data = np.nan*np.ones((1,len(lat),len(lon)))
    else:
      raise Exception('Error pmap: map_on_network works with not None data.')
    grid_coord = False
  nlon,nlat = len(lon),len(lat)
  if len(data.shape)<3: data = data[np.newaxis]
  latinvert = (lat[0]<lat[-1])
  if latinvert:
    lat,data = lat[::-1],data[:,::-1]
    if 'fdir' in kwargs: kwargs['fdir'] = kwargs['fdir'][::-1]
    if 'facc' in kwargs: kwargs['facc'] = kwargs['facc'][::-1]

  Ilon,Ilat = get_IlonIlat(lon,lat,zone)
  lon,lat,data = lon[Ilon],lat[Ilat],data[:,Ilat,Ilon]
  if mask is not None:
    if latinvert: mask = mask[::-1]
    Jmask,Imask = np.where(~(mask.astype(bool)))
    data[:,Jmask,Imask] = np.nan
  allnan = np.all(np.isnan(data)) if np.isnan(MV) else np.all(data==MV)
  extent = [lon[0]-res_lon/2.,lon[-1]+res_lon/2.,lat[-1]-res_lat/2.,lat[0]+res_lat/2.]

  if projection is None: projection = ccrs.PlateCarree()
  elif isinstance(projection,int): projection = EPSG(projection)
  elif isinstance(projection,str):
    if   projection.lower()=='platecarree': projection = ccrs.PlateCarree()
    elif projection.lower()=='l2e': projection = L2E_Proj()#ccrs.epsg(27572)
    elif projection.lower()=='robinson': projection = ccrs.Robinson()
    elif projection.lower()=='mollweide': projection = ccrs.Mollweide()
    elif projection.lower()=='orthographic': projection = ccrs.Orthographic()
    elif projection.lower()=='europp': projection = ccrs.EuroPP()

  if ax is None or 'projection' not in dir(ax):
    kwargs_ct = available_kwargs(geoaxes.GeoAxes.__init__,kwargs)
    if 'get_position' in dir(ax):
      pos = ax.get_position()
      ax.remove()
      ax = plt.axes(pos,projection=projection,**kwargs_ct)
    else:
      ax = plt.axes(projection=projection,**kwargs_ct)
    ax.set_extent(zone[[0,1,1,0],[0,0,1,1]],projection)

  # NaturalEarth
  if naturalearth and not ('naturalearth' in ax.__dict__ and ax.naturalearth):
    if projection==ccrs.PlateCarree():
      if isinstance(naturalearth,str) and naturalearth.lower()=='ne1':
        ne = np.memmap(myPath[0]+'/data/NE1_HR_LC_SR_W.bin','u1','r',shape=(10800,21600,3))
      else:
        ne = np.memmap(myPath[0]+'/data/NE2_HR_LC_SR_W_DR.bin','u1','r',shape=(10800,21600,3))
      lon0,lat0 = res2lonlat(1./60)
      Ilon,Ilat = get_IlonIlat(lon0,lat0,zone)
      hne = FastImshow(ne[Ilat,Ilon,...],ax=ax,interpolation='bicubic',origin='upper',
                       extent=zone[[0,1,1,0],[0,0,1,1]])
      hne.buf,hne.sz,hne.extent,hne.startx,hne.starty = ne,ne.shape,np.array((-180,180,-90,90)),-180,-90
      hne.im.zorder = -1
      ax.naturalearth = True
      if coast is None: coast = False
    else:
      print('NaturalEarth is not available yet for projections different than WGS84.')

  # Map details
  if river and not ('river' in ax.__dict__ and ax.river):
    ax.add_feature(cfeature.NaturalEarthFeature('physical','rivers_lake_centerlines','10m'),
                   facecolor='none',edgecolor='blue',linewidth=1,zorder=2)
    ax.river = True
  if coast is None:
    if 'naturalearth' in ax.__dict__:
      coast = not ax.naturalearth
    else:
      coast = not naturalearth
  if coast and not ('coast' in ax.__dict__ and ax.coast):
    ax.coastlines(resolution='10m')
    ax.coast = coast

  # Color scale
  #if MV is not np.nan: data[data==MV] = np.nan
  if vlim is not None: vmin,vmax = vlim
  else:
    if 'vmin' in kwargs: vmin = kwargs['vmin']
    else: vmin = 0 if allnan else np.nanmin(data[data!=MV])
    if 'vmax' in kwargs: vmax = kwargs['vmax']
    else: vmax = 1 if allnan else np.nanmax(data[data!=MV])

  # Colormap
  # cm can be [jet,seismic_r,BrBG,terrain]
  class FixPointNormalize(colors.Normalize):
      def __init__(self):
          colors.Normalize.__init__(self, vmin, vmax)
      def __call__(self, value, clip=None):
          x, y = [self.vmin, 0, self.vmax], [0, 0.21875, 1]
          return np.ma.masked_array(np.interp(value, x, y))
  if norm is None:
    if cm=='terrain': norm = FixPointNormalize()
    else: norm = colors.Normalize(vmin=vmin,vmax=vmax,clip=True)
  if isinstance(cm,str):
    cmap = plt.get_cmap(cm)
    cmap.set_bad(alpha=0)
  else:
    cmap = cm

  if not map_on_network:
    hp = FastImshow(data[0,:,:], *args, ax=ax, origin='upper', extent=extent,
                    norm=norm, cmap=cmap, MV=MV, interpolation=interpolation,
                    **kwargs)
  else:
    kwargs_rc = available_kwargs(RiverCollection,kwargs)
    hp = RiverCollection(zone,mask=mask,norm=norm,cmap=cmap,**kwargs_rc)
    hp.set_array(data[0,hp.J,hp.I])
    Inan = np.isnan(data[0,hp.J,hp.I])+(data[0,hp.J,hp.I]==MV)
    hp.set_linewidth(np.where(Inan,0,hp.rw))
    #hp.set_alpha(norm(data[0,hp.J,hp.I]))
    ax.add_collection(hp)
  hp.lon,hp.lat,hp.data = lon,lat,data

  if data.shape[0] == 1:
    hp.axes.set_title(title)

  elif data.shape[0] > 1:
    hp.Ntime = data.shape[0]
    hp.ktime = 0
    isdat = False
    if dat is None: dat,isdat = np.arange(hp.Ntime),True

    def __redraw():
      if isdat: hp.axes.set_title(title+' - {:d}/{:d}'.format(
                                  hp.ktime+1,hp.Ntime))
      else    : hp.axes.set_title(title+' - {:s} [{:d}/{:d}]'.format(
                                  dat[hp.ktime].strftime('%Y-%m-%d %H:%M:%S'),hp.ktime+1,hp.Ntime))
      if not map_on_network:
        hp.buf = hp.data[hp.ktime,:,:]
        hp.ax_update(ax)
      else:
        hp.set_array(data[hp.ktime,hp.J,hp.I])
        Inan = np.isnan(data[hp.ktime,hp.J,hp.I])+(data[hp.ktime,hp.J,hp.I]==MV)
        hp.set_linewidth(np.where(Inan,0,hp.rw))
        #hp.set_alpha(norm(data[0,hp.J,hp.I]))
        fig.canvas.draw_idle()

    def __change_date(event):
      if   event.key == 'up'   : hp.ktime = 0
      elif event.key == 'right': hp.ktime = np.mod(hp.ktime+1,hp.Ntime)
      elif event.key == 'left' : hp.ktime = np.mod(hp.ktime-1,hp.Ntime)
      elif event.key == 'down' : hp.ktime = hp.Ntime-1
      __redraw()

    def __plot_ts(event):
      if not event.dblclick: return
      fig = [manager.canvas.figure
          for manager in mpl._pylab_helpers.Gcf.get_all_fig_managers()
          if 'tag' in manager.canvas.figure.__dict__
          and manager.canvas.figure.tag==id(hp)]
      if len(fig)==0:
        fig = plt.figure()
        fig.tag = id(hp)
        axts = fig.add_subplot(111)
        axts.set_title(title)
      else:
        axts = fig[0].axes[0]
      i,j = get_IlonIlat(lon,lat,(event.xdata,event.ydata))
      axts.plot(dat,data[:,j,i],label='x={:g} [{:d}]  y={:g} [{:d}]'.format(lon[i],i,lat[j],j))[0]
      axts.legend()
      axts.figure.canvas.draw_idle()

    fig = plt.gcf()
    #if 'key_press_event' in fig.canvas.callbacks.callbacks:
    #  fig.canvas.callbacks.callbacks.pop('key_press_event')
    fig.canvas.mpl_connect('key_press_event',__change_date)
    #if 'button_press_event' in fig.canvas.callbacks.callbacks:
    #  fig.canvas.callbacks.callbacks.pop('button_press_event')
    fig.canvas.mpl_connect('button_press_event',__plot_ts)

    __redraw()
    hp.__redraw = __redraw

    #hp.axes.axis(zone[[0,1,1,0],[0,0,1,1]])
    if not map_on_network:
      hp.im.set_clim(vmin,vmax)
      format_data(hp.im)

  else:
    hp = None

  if grid_coord: set_grid_coord(ax,lon,lat,latinvert=latinvert,h=hp)

  if   nargout() == 1: return hp
  elif nargout() == 2: return hp,ax
  #return hp,ax


def def_zone(region=None):
  '''
  Default zones
  '''
  if region is None: region = 'Globe'
  zone = None
  region = region.lower()
  if   region=='globe' : zone = [[-180,90],[180,-90]]
  elif region=='france': zone = [[-5,51.5],[8.5,41.5]]
  elif region=='spain' : zone = [[-9.5,44],[3.5,35.5]]
  elif region=='europe': zone = [[-11,71],[32,35]]
  return zone


def set_grid_coord(ax,axlon,axlat,latinvert=False,h=None):
  '''
  Set figure coordinates to center cells of gridded map
  '''
  if latinvert: axlat = axlat[::-1]
  def format_coord(x,y):
    i,j = get_IlonIlat(axlon,axlat,(x,y))
    k = h.ktime if h.data.shape[0]>1 else 0
    val = h.data[k,::-1][j,i] if isinstance(h,LineCollection) else np.nan
    val = '    [{:g}]'.format(val) if np.isfinite(val) else ''
    return 'x={:g} [{:d}]    y={:g} [{:d}]'.format(axlon[i],i,axlat[j],j)+val
  ax.format_coord = format_coord


#def oceanmask():
  '''
  import matplotlib as mpl
  from mpl_toolkits.basemap import Basemap
  from matplotlib.patches import Path, PathPatch
  map_color="#000000"
  map = Basemap(llcrnrlon=lon_ll,llcrnrlat=lat_ll,urcrnrlon=lon_ur,urcrnrlat=lat_ur,
                projection='cyl',resolution ='l',area_thresh=1000.)
  map.drawcoastlines(linewidth=1.,color=map_color)
  map.drawcountries(linewidth=.5,color=map_color)
  map.drawstates(linewidth=0.5,color=map_color)
  #map.fillcontinents(color='coral',lake_color='aqua')
  # draw parallels and meridians.
  map.drawparallels(np.arange(-80.,81.,10.),color='#aaaaaa')
  map.drawmeridians(np.arange(-180.,181.,10.),color='#aaaaaa')
  #map.drawmapboundary(fill_color='aqua')
  #--------------------------------------------------------
  # Mask Oceans...
  #--------------------------------------------------------
  # first, hex color:
  color_ocean = "#ffffff"
  if map_rgb_ocean != 'NA' :
  #getting the limits of the map:
  x0,x1 = ax.get_xlim()
  y0,y1 = ax.get_ylim()
  map_edges = np.array([[x0,y0],[x1,y0],[x1,y1],[x0,y1]])
  #getting all polygons used to draw the coastlines of the map
  polys = [p.boundary for p in map.landpolygons]
  #combining with map edges
  polys = [map_edges]+polys[:]
  #creating a PathPatch
  codes = [[Path.MOVETO] + [Path.LINETO for p in p[1:]] for p in polys]
  polys_lin = [v for p in polys for v in p]
  codes_lin = [c for cs in codes for c in cs]
  path      = Path(polys_lin, codes_lin)
  patch     = PathPatch(path,facecolor=color_ocean, lw=0)
  #masking the data:
  ax.add_patch(patch) 
  '''

def utm2zone(utm):
  '''
  Get zone from UTM
  '''
  zone = np.empty((2,2))
  zone[0,0] = (float(UTM[2:4])-31)*6
  zone[1,0] = zone[0,0]+6
  lat = (ord(UTM[1].upper())-65)*4
  zone[:,1] = [lat,lat+4] if UTM[0]=='N' else [-lat,-lat+4]
  return zone

def pnt2utm(pnt):
  '''
  Get UTM from point
  '''
  UTM = 'N' if pnt[1]>=0 else 'S'
  UTM = UTM+chr(65+int(np.abs(pnt[1])/4))
  UTM = UTM+'{:02d}'.format(1+int((pnt[0]+180)/6))
  return UTM

def zone2utm(zone):
  '''
  Get all UTMs covering zone
  '''
  zone = np.array(zone)
  x = np.arange(np.floor(zone[0,0]/6)*6+3,np.floor(zone[1,0]/6)*6+3,6)
  y = np.arange(np.floor(zone[1,1]/4)*4+2,np.floor(zone[0,1]/4)*4+2,4)
  X,Y = np.meshgrid(x,y)
  UTM = []
  for xx,yy in zip(X.flatten(),Y.flatten()):
    UTM.append(pnt2utm((xx,yy)))
  return UTM


class EPSG(ccrs.Projection):
  # WGS83: 4326
  # L93: 2154
  # L2E: 27572
  def __init__(self,code):
    from pyproj import CRS
    proj = CRS('epsg:'+str(code))
    globe = ccrs.Globe(semimajor_axis='6378249.2',semiminor_axis='6356515',towgs84='-168,-60,320,0,0,0,0')
    super(EPSG, self).__init__(proj.to_dict(),globe)
    x0, y0, x1, y1 = proj.area_of_use.bounds
    geodetic = ccrs.Geodetic()
    lons = np.array([x0, x0, x1, x1])
    lats = np.array([y0, y1, y1, y0])
    points = self.transform_points(geodetic, lons, lats)
    x,y = points[:, 0],points[:, 1]
    self.bounds = (x.min(), x.max(), y.min(), y.max())
  @property
  def boundary(self):
    import shapely.geometry as sgeom
    x0, x1, y0, y1 = self.bounds
    return sgeom.LineString([(x0, y0), (x0, y1), (x1, y1), (x1, y0), (x0, y0)])
  @property
  def x_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (x0, x1)
  @property
  def y_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (y0, y1)
  @property
  def threshold(self):
      x0, x1, y0, y1 = self.bounds
      return min(x1 - x0, y1 - y0) / 100.

class L2E_Proj(ccrs.Projection):
  def __init__(self):
    proj4_dict = {'proj':'lcc','lat_1':'46.8','lat_0':'46.8','lon_0':'0','k_0':'0.99987742',
                  'x_0':'600000','y_0':'2200000','pm':'paris','units':'m','no_defs':None}
    globe = ccrs.Globe(semimajor_axis='6378249.2',semiminor_axis='6356515',towgs84='-168,-60,320,0,0,0,0')
    super(L2E_Proj, self).__init__(proj4_dict,globe)
    x0, x1, y0, y1 = [-4.87, 8.23, 42.33, 51.14]
    geodetic = ccrs.Geodetic()
    lons = np.array([x0, x0, x1, x1])
    lats = np.array([y0, y1, y1, y0])
    points = self.transform_points(geodetic, lons, lats)
    x,y = points[:, 0],points[:, 1]
    self.bounds = (x.min(), x.max(), y.min(), y.max())
  @property
  def boundary(self):
    import shapely.geometry as sgeom
    x0, x1, y0, y1 = self.bounds
    return sgeom.LineString([(x0, y0), (x0, y1), (x1, y1), (x1, y0), (x0, y0)])
  @property
  def x_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (x0, x1)
  @property
  def y_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (y0, y1)
  @property
  def threshold(self):
      x0, x1, y0, y1 = self.bounds
      return min(x1 - x0, y1 - y0) / 100.


def RiverCollection(zone,res=None,mask=None,norm=None,cmap=None,fdir=None,facc=None,conv_db='trip',
                    facc_min=1,width=None,width_min=None,width_factor=1,Nmax=None):
  '''
  '''
  if res is None: res = 1/12
  lon,lat = res2lonlat(res,zone)
  lon_ext = np.array([lon[0]-res]+lon.tolist()+[lon[-1]+res])
  lat_ext = np.array([lat[0]+res]+lat.tolist()+[lat[-1]-res])
  next_i,next_j = convention(conv_db)
  if fdir is None: fdir = trip_param('flowdir',zone)
  if facc is None: facc = dir2acc(fdir,conv_db=conv_db)
  if conv_db.lower()=='drt':
    fdir = np.where((fdir>0)*(fdir<=128),np.log2(fdir)+1,0).astype(int)
  if mask is None: mask = np.ones(fdir.shape,bool)
  if width is None: width = trip_param('widthriv',zone)
  if width_min is not None: mask[width<width_min] = 0
  mask[facc<facc_min] = 0
  mask[(fdir<1)+(fdir>8)] = 0
  J,I = np.where(mask)
  N = mask.sum()
  width_max = width[J,I].max()
  facc_max = facc[J,I].max()
  c = 0.25
  alpha = (4/0.1)**(1/c)
  b = (alpha*facc_min-facc_max)/(alpha-1)
  a = 4/(alpha/(alpha-1)*(facc_max-facc_min))**c
  Nseg,seg = fbasin.river_segments(lon,lat,fdir,conv_db)
  seg = np.array(seg[:Nseg])
  I,J = seg[:,0,0],seg[:,0,1]
  rxy = np.concatenate((lon_ext[seg[:,:,0]+1][:,:,np.newaxis],lat_ext[seg[:,:,1]+1][:,:,np.newaxis]),axis=2)
  if width_min is None:
    rw = a*(facc[J,I]-b)**c
  else:
    rw = 4-(4-.1)*(np.log(width_max/width[J,I])/np.log(width_max/width_min))
  rw = width_factor*rw
  if Nmax is not None: Nseg = min(Nseg,Nmax)
  K = np.argsort(rw)[::-1][:Nseg][::-1]
  I,J,rxy,rw = I[K],J[K],rxy[K],rw[K]
  lc = LineCollection(rxy,linewidths=rw)
  lc.set_capstyle('round')
  lc.N,lc.I,lc.J,lc.rw = K.sum(),I,J,rw
  if norm is not None: lc.set_norm(norm)
  if cmap is not None: lc.set_cmap(cmap)
  return lc


def draw_trip_network(outlet=None,ax=None,zone=None,res=1./12,naturalearth=True,
                      data={},to_draw=['fdir','outlet'],inbasin=True,Nbasin=100,mapmask=None,conv_db='trip',
                      river_color=[0,0,1],width_factor=1,tacc=None,boundary_color='g',
                      **kwargs):
  '''
  Plot river network for given outlets or for a given zone
  '''
  if zone is None:
    try:
      ex = ax.get_extent()
      zone = [[ex[0],ex[3]],[ex[1],ex[2]]]
    except:
      zone = [[-180,90],[180,-90]]
  elif isinstance(zone,str):
    zone = def_zone(zone)
  else:
    zone = [[np.floor(zone[0][0]/res+.1*res)*res,np.ceil(zone[0][1]/res-.1*res)*res],
            [np.ceil(zone[1][0]/res-.1*res)*res,np.floor(zone[1][1]/res+.1*res)*res]]
  lon,lat = res2lonlat(res,zone=zone)
  nlon,nlat = len(lon),len(lat)
  # Initialize map
  pmap_kwargs = available_kwargs(pmap,kwargs)
  if ax is None or 'projection' not in dir(ax):
    ax = pmap(ax=ax,zone=zone,naturalearth=naturalearth,**pmap_kwargs)
  #else:
  #  ax = pmap(ax=ax,zone=zone,naturalearth=naturalearth,**pmap_kwargs)
  # Get TRIP parameters
  if 'fdir' not in data or data['fdir'].shape!=(nlat,nlon):
    data['fdir'] = trip_param('flowdir',zone=zone,res=res)
  if 'facc' not in data or data['facc'].shape!=(nlat,nlon):
    #data['facc'] = trip_param('dr_area',zone=zone,res=res)
    data['facc'] = dir2acc(data['fdir'],conv_db=conv_db)
  # Get outlets and corresponding masks
  if outlet is not None:
    outlet = np.array(outlet).reshape((-1,2))
    if outlet.size==2:
      mask = basin_mask(lon,lat,data['fdir'],outlet,conv_db=conv_db)
    else:
      #facc_masked = data['facc'].copy()
      mask = np.zeros((nlat,nlon))
      for n,ol in enumerate(outlet.tolist()):
        new_mask = basin_mask(lon,lat,data['fdir'],ol,conv_db=conv_db)
        #facc_masked[new_mask>0] = 0
        #if mapmask is None or (new_mask*(1-mapmask[lat==ol[1],lon==ol[0]]>0: # outlet in mapmask
        if mapmask is None or (new_mask*(1-mapmask)).sum()==0: # newmask included in mapmask
          n = n+1
          mask = mask+n*new_mask
  else:
    facc_masked = data['facc'].copy()
    if mapmask is None and \
        not any([s in to_draw for s in ['mask','bnd','mainstream','upstream','downstream','network']]):
      inbasin,Nbasin = False,0
    n = 0
    outlet = np.zeros((0,2))
    if mapmask is None: mask = np.ones((nlat,nlon))
    while (Nbasin is None or n<Nbasin) and np.nansum(facc_masked)>0:
      j,i = np.where(facc_masked==np.nanmax(facc_masked))
      j,i = j[0],i[0]
      ol = np.array((lon[i],lat[j])).reshape((1,2))
      if mapmask is None or mapmask[j,i]>0:
        outlet = np.vstack((outlet,ol))
        n = n+1
      new_mask = basin_mask(lon,lat,data['fdir'],ol,conv_db=conv_db)
      facc_masked[new_mask>0] = 0
      if mapmask is None: mask = mask+n*new_mask
  if mapmask is not None: mask = mapmask
  # Draw DEM
  if 'dem' in to_draw:
    if 'dem' not in data or data['dem'].shape!=(nlat,nlon):
      data['dem'] = trip_param('topo_riv',zone=zone,res=res)
    pmap(lon,lat,data['dem'],cm='terrain',ax=ax,coast=False,**pmap_kwargs)
  # Draw masks
  if 'mask' in to_draw:
    cmap = mpl.cm.Set1(plt.Normalize(0,10)(list(range(11))))
    cmap = np.vstack(np.ceil(n/11.).astype(int)*(cmap,))[:n,:]
    cmap = colors.ListedColormap(cmap,N=n)
    pmap(lon,lat,mask,MV=0,alpha=0.3,ax=ax,cm=cmap,**pmap_kwargs)
  # Loop over basins
  for n,ol in enumerate(outlet.tolist()):
    # Draw boundary
    if 'bnd' in to_draw:
      data['bnd'] = mask2bnd(lon,lat,1*(mask==n+1))
      plt.plot(*data['bnd'].T,boundary_color)
    # Draw mainstream (up- and down-)
    if 'mainstream' in to_draw:
      to_draw = list(to_draw)+['upstream','downstream']
    if 'upstream' in to_draw:
      upstream = main_upstream(lon,lat,data['fdir'],data['facc'],ol,conv_db=conv_db)
      plt.plot(*upstream.T,'b')
    if 'downstream' in to_draw:
      downstream = main_downstream(lon,lat,data['fdir'],ol,conv_db=conv_db)
      plt.plot(*downstream.T,'b')
    # Draw network
    if 'network' in to_draw:
      Nmax = None if 'Nmax' not in kwargs else kwargs['Nmax']
      Smin = None if 'Smin' not in kwargs else kwargs['Smin']
      rn_kwargs = available_kwargs(river_network,kwargs)
      RN = river_network(lon,lat,data['fdir'],data['facc'],ol,conv_db=conv_db,**rn_kwargs)
      lwmin,lwmax = 0.2,4
      for nw in RN:
        plt.plot(*nw['route'].T,'b',lw=(lwmax-lwmin)*np.exp(-nw['id']/2.)+lwmin)
  # Draw flow directions
  if 'fdir' in to_draw:
    def remove_nan(v,step=1):
      v = v[np.argmax(~np.isnan(v[:,0])):]
      Irm = [i for i in range(len(v)-step) if np.isnan(v[i,0])*np.isnan(v[i+step,0])]
      return np.delete(v,Irm,axis=0)
    lon_ext = np.array([lon[0]-res]+lon.tolist()+[lon[-1]+res])
    lat_ext = np.array([lat[0]+res]+lat.tolist()+[lat[-1]-res])
    next_i,next_j = convention(conv_db)
    # Apply log2 for DRT convention
    fdir = data['fdir'].copy()
    if conv_db.lower()=='drt':
      I = (fdir>0)*(fdir<=128)
      fdir[I] = np.log2(fdir[I]).astype('u1')+1
    #
    if False:
      reaches = np.nan*np.zeros((fdir.size*3,2))
      added_outlets = np.nan*np.zeros((fdir.size,2))
      for j in range(nlat):
        for i in range(nlon):
          if inbasin and mask[j,i]==0: continue
          if fdir[j,i]==9: added_outlets[i+j*nlon,:] = [lon[i],lat[j]]
          if not(fdir[j,i]>0 and fdir[j,i]<9): continue
          ni,nj = i+next_i[fdir[j,i]-1],j+next_j[fdir[j,i]-1]
          #if ni<0 or ni>=nlon or nj<0 or nj>=nlat: continue
          reaches[3*(i+j*nlon):3*(i+j*nlon)+2,:] = [[lon_ext[i+1],lat_ext[j+1]],[lon_ext[ni+1],lat_ext[nj+1]]]
      reaches = remove_nan(reaches,1)
      added_outlets = remove_nan(added_outlets,0)
      plt.plot(*reaches.T,'b')
      plt.plot(*added_outlets.T,'o',ms=4,color='0.5')
    #
    else:
      if tacc is None:
        if res==0.5:
          tacc = [0,1,3,8,50,100000] # HD full
        else:
          #tacc = [0,8,30,80,500,100000] # 12D full
          tacc = [8,50,100,500,1000,100000] # 12D dense
          #tacc = [50,100,500,1000,5000,100000] # 12D sparse
      elif isinstance(tacc,str):
        if   tacc=='sparse': tacc = [50,100,500,1000,5000,100000] # 12D sparse
        elif tacc=='full'  : tacc = [0,8,30,80,500,100000] # 12D full
        else               : tacc = [8,50,100,500,1000,100000] # 12D dense
      Nacc = len(tacc)-1
      if isinstance(river_color,str) and river_color in colors.get_named_colors_mapping():
        river_color = colors.get_named_colors_mapping()[river_color]
      norm = colors.Normalize(vmin=-1.5*(Nacc-1),vmax=Nacc-1)
      cmap = colors.LinearSegmentedColormap.from_list('river',[[1,1,1],river_color],N=2*Nacc+1)
      for i in range(Nacc):
        J,I = np.where((mask>0)*(fdir>0)*(fdir<9)*(data['facc']>=tacc[i])*(data['facc']<tacc[i+1]))
        rx = np.vstack([lon[I],lon_ext[I+next_i[fdir[J,I]-1]+1],np.nan*np.ones((len(I),))]).T.flatten()
        ry = np.vstack([lat[J],lat_ext[J+next_j[fdir[J,I]-1]+1],np.nan*np.ones((len(I),))]).T.flatten()
        plt.plot(rx,ry,'-',color=cmap(norm(1.*i)),lw=width_factor*(.7+.8*i),alpha=.8)[0]
        # Pour une projection L2E
        #from cartopy import crs as ccrs
        #WGS = ccrs.PlateCarree()
        #L2E = ccrs.epsg(27572)
        #xy = L2E.transform_points(WGS,rx,ry)[:,:2]
        #plt.plot(*xy.T,'-',color=cmap(norm(1.*i)),lw=width_factor*(.7+.8*i),alpha=.8)
  # Draw basin outlets
  if 'outlet' in to_draw: plt.plot(*outlet.T,'go',ms=6)
  # Returns data if required
  if nargout()== 1:
    output = {}
    for k,v in data.items(): output[k] = v.copy()
    return output


def draw_merit_network(zone,width_min=500,**kwargs):
  lon,lat,fdir = merit_param('dir',zone)
  lon,lat,facc = merit_param('upg',zone)
  hp,ax = pmap(lon,lat,facc,zone=zone,map_on_network=True,res=1/1200,
               fdir=fdir,facc=facc,conv_db='drt',width=facc,width_min=width_min,**kwargs)
  if   nargout() == 1: return hp
  elif nargout() == 2: return hp,ax






