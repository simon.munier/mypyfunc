import numpy as np
from sys import setrecursionlimit
from os import path
from netCDF4 import Dataset
from .tqdm import tqdm,trange
from .misc import nargout,available_kwargs,spatial_area,earth_radius
from .misc import res2lonlat,get_IlonIlat,convention
#from .contourc import contourc

# Compiled Fortran modules
from .mod_basin import basin as fbasin
from .mod_merit import merit as fmerit

setrecursionlimit(10000)

trip_pgd_dir = '/cnrm/surface/munier/Data/Routing/TRIP/'
#trip_pgd_dir = '/home/muniers/Data/ECOCLIMAP/TRIP/'
merit_dir =  '/cnrm/surface/munier/Data/Routing/MERIT/'
#merit_dir = '/home/muniers/Data/Routing/MERIT/'

def trip_param(param,zone=[[-180,90],[180,-90]],res=1./12,tripfile=None,continent=None):
  '''
  FLOWDIR,RIVSEQ,NUM_BAS,DR_AREA,SLOPERIV,WIDTHRIV,N_RIV,RIVDEPTH,
  NFLOOD,NUM_AQUI,TOPO_RIV,WEFF,TRANS,GREEN_ANT,FRACAREA
  QMEAN
  '''
  zone = np.array(zone)
  if len(zone.shape)==1: zone = [[zone[0]-res/2,zone[1]+res/2],[zone[0]+res/2,zone[1]-res/2]]
  #lon,lat = res2lonlat(res,zone)
  lon,lat = res2lonlat(res)
  Ilon,Ilat = get_IlonIlat(lon,lat,zone)
  lon,lat = lon[Ilon],lat[Ilat]
  if res==1: resstr = '1D'
  elif res==0.5: resstr = 'HD'
  else: resstr = '12D'
  if tripfile is None:
    if continent is None:
      tripfile = trip_pgd_dir+'/TRIP_PGD_'+resstr+'.nc'
    else:
      tripfile = trip_pgd_dir+'/TRIP_PGD_'+resstr+'_'+continent+'.nc'
  if param.upper()=='QMEAN':
    with Dataset(trip_pgd_dir+'Qmean_12D_GGHYDRO.nc') as nc:
      tlon,tlat = nc.variables['longitude'][:],nc.variables['latitude'][:]
      Ilon,Ilat = get_IlonIlat(tlon,tlat,zone)
      data = nc.variables['Qmean'][Ilat,Ilon]
  else:
    with Dataset(tripfile) as nc:
      tlon,tlat = nc.variables['longitude'][:],nc.variables['latitude'][:]
      Ilon,Ilat = get_IlonIlat(tlon,tlat,zone)
      data = nc.variables[param.upper()][Ilat,Ilon]
  if tlat[1]>tlat[0]: data = data[::-1,:]
  if np.ma.isMA(data): data = data.filled(np.nan)
  if param.upper()=='FLOWDIR': data = data.astype('int8')
  if nargout()==1: return data
  else           : return lon,lat,data


def merit_param(param,zone,MV=np.nan):
  '''
  '''
  maindir = merit_dir+'/'+param+'_3s/'
  merit_type = {'elv':'<f4','dir':'u1','upg':'i4','upa':'<f4','wth':'<f4','org':'u1'}
  merit_type['river0'] = 'u4'
  res = 1./1200
  zone = np.array(zone)
  if len(zone.shape)==1: zone = [[zone[0]-res/2,zone[1]+res/2],[zone[0]+res/2,zone[1]-res/2]]
  #lon,lat = res2lonlat(res,zone)
  lon,lat = res2lonlat(res)
  Ilon,Ilat = get_IlonIlat(lon,lat,zone)
  lon,lat = lon[Ilon],lat[Ilat]
  Xtile = np.arange(np.floor(zone[0][0]/5.)*5,np.ceil(zone[1][0]/5.)*5,5)
  Ytile = np.arange(np.floor(zone[1][1]/5.)*5,np.ceil(zone[0][1]/5.)*5,5)
  data = None
  for y0 in Ytile[::-1]:
    datay = None
    for x0 in Xtile:
      tile = '{:s}{:02.0f}{:s}{:03.0f}'.format('n' if y0>=0 else 's',np.abs(y0),'e' if x0>=0 else 'w',np.abs(x0))
      x,y = res2lonlat(res,[[x0,y0+5],[x0+5,y0]])
      IX,IY = get_IlonIlat(x,y,zone)
      if path.isfile(maindir+tile+'_'+param+'.bin'):
        tmp = np.memmap(maindir+tile+'_'+param+'.bin',merit_type[param],shape=(6000,6000),mode='r')[IY,IX]
      else:
        tmp = np.memmap(merit_dir+'/empty_'+merit_type[param].replace('<','')+'.bin',
                        merit_type[param],shape=(6000,6000),mode='r')[IY,IX]
      if datay is None: datay = tmp
      else: datay = np.hstack((datay,tmp))
    if data is None: data = datay
    else: data = np.vstack((data,datay))
  if nargout()==1: return data
  else           : return lon,lat,data


def tiled_param(maindir,param,zone,dtype='<f4',MV=np.nan,res=1./1200):
  '''
  '''
  if maindir[-1]!='/': maindir = maindir+'/'
  zone = np.array(zone)
  if len(zone.shape)==1: zone = [[zone[0]-res/2,zone[1]+res/2],[zone[0]+res/2,zone[1]-res/2]]
  lon,lat = res2lonlat(res,zone)
  Xtile = np.arange(np.floor(zone[0][0]/5.)*5,np.ceil(zone[1][0]/5.)*5,5)
  Ytile = np.arange(np.floor(zone[1][1]/5.)*5,np.ceil(zone[0][1]/5.)*5,5)
  data = None
  for y0 in Ytile[::-1]:
    datay = None
    for x0 in Xtile:
      tile = '{:s}{:02.0f}{:s}{:03.0f}'.format('n' if y0>=0 else 's',np.abs(y0),'e' if x0>=0 else 'w',np.abs(x0))
      x,y = res2lonlat(res,[[x0,y0+5],[x0+5,y0]])
      IX,IY = get_IlonIlat(x,y,zone)
      if path.isfile(maindir+tile+'_'+param+'.bin'):
        tmp = np.memmap(maindir+tile+'_'+param+'.bin',dtype,shape=(6000,6000),mode='r')[IY,IX]
      else:
        tmp = np.nan*np.zeros((IY.stop-IY.start,IX.stop-IX.start),dtype)
      if datay is None: datay = tmp
      else: datay = np.hstack((datay,tmp))
    if data is None: data = datay
    else: data = np.vstack((data,datay))
  return data


def basin_mask(lon,lat,fdir,outlet,conv_db='trip'):
  '''
  Computes basin mask at outlet from flow direction grid
  mask = basin_mask(lon,lat,fdir,outlet)
  '''
  f = fdir.copy()
  # Flip if increasing latitudes
  if lat[1]>lat[0]:
    lat = lat[::-1]
    f = f[::-1,:]
    flip_lat = True
  else:
    flip_lat = False
  # Get outlet indices
  outlet = np.array(outlet).flatten()
  i0 = np.argmin(np.abs(lon-outlet[0]))
  j0 = np.argmin(np.abs(lat-outlet[1]))
  # Apply log2 for DRT convention
  if conv_db.lower()=='drt':
    I = (f>0)*(f<=128)
    f[I] = np.log2(f[I]).astype('u1')+1
  # Compute mask
  mask = fbasin.basin_mask(f,(i0,j0),conv_db.lower())
  if flip_lat: mask = mask[::-1,:]
  return(mask)


def basin_mask_py(lon,lat,fdir,outlet):
  f = fdir.copy()
  # Get outlet indices
  outlet = np.array(outlet).flatten()
  i0 = np.argmin(np.abs(lon-outlet[0]))
  j0 = np.argmin(np.abs(lat-outlet[1]))
  # Compute mask
  next_i,next_j = convention(latinvert=(lat[1]>lat[0]))
  mask = np.zeros(fdir.shape,dtype=int)
  def previous_ij(i,j):
    mask[j,i] = 1
    for k in range(8):
      pi,pj = i-next_i[k],j-next_j[k]
      if pj<0 or pj>=len(lat) or pi<0 or pi>=len(lon): continue
      if fdir[pj,pi]==k+1: previous_ij(pi,pj)
  previous_ij(i0,j0)
  return(mask)


def basin_mask_trip(outlet,zone=[[-180,90],[180,-90]],res=1./12):
  '''
  Computes basin mask at outlet from CTRIP flow direction grid
  mask = basin_mask_trip(outlet)
  lon,lat,mask = basin_mask_trip(outlet)
  '''
  lon,lat = res2lonlat(res,zone=zone)
  fdir = trip_param('flowdir',zone,res)
  mask = basin_mask(lon,lat,fdir,outlet)
  if nargout()==1: return mask
  else           : return lon,lat,mask


def basin_mask_merit(outlet,zone=None):
  '''
  Computes basin mask at outlet from native MERIT flow direction grid
  mask = basin_mask_merit(outlet)
  lon,lat,mask = basin_mask_merit(outlet)
  '''
  fmerit.basin_mask(outlet)
  if zone is not None:
    lon,lat = res2lonlat(1/1200.,zone=zone)
    Ilon = slice(np.argmin(np.abs(lon-fmerit.lon[0])),np.argmin(np.abs(lon-fmerit.lon[-1]))+1)
    Ilat = slice(np.argmin(np.abs(lat-fmerit.lat[0])),np.argmin(np.abs(lat-fmerit.lat[-1]))+1)
    mask = np.zeros((len(lat),len(lon)),'u1')
    mask[Ilat,Ilon] = fmerit.mask
  else:
    lon,lat,mask = fmerit.lon.copy(),fmerit.lat.copy(),fmerit.mask.copy()
  if nargout()==1: return mask
  else           : return lon,lat,mask


def main_upstream(lon,lat,fdir,facc,outlet,conv_db='trip'):
  '''
  Returns the main river path from outlet
  '''
  f = fdir.copy()
  # Flip if increasing latitudes
  if lat[1]>lat[0]:
    lat = lat[::-1]
    f = f[::-1,:]
    facc = facc[::-1,:]
  # Get outlet indices
  i0 = np.argmin(np.abs(lon-outlet[0]))
  j0 = np.argmin(np.abs(lat-outlet[1]))
  # Apply log2 for DRT convention
  if conv_db.lower()=='drt':
    I = (f>0)*(f<=128)
    f[I] = np.log2(f[I]).astype('u1')+1
  # Upstream route
  N,route_ij = fbasin.main_upstream(f,facc,(i0,j0),conv_db)
  route_ij = np.array(route_ij[:N])
  route = np.vstack((lon[route_ij[:,0]],lat[route_ij[:,1]])).T
  return route


def main_upstream_trip(outlet,res=1./12):
  '''
  Returns the main river path from outlet using CTRIP network
  '''
  lon,lat = res2lonlat(res)
  fdir = trip_param('flowdir',res=res)
  facc = trip_param('dr_area',res=res)
  route = main_upstream(lon,lat,fdir,facc,outlet)
  return route


def main_upstream_merit(outlet,zone=None):
  '''
  Returns the main river path from outlet using the native MERIT dem
  '''
  fmerit.main_upstream(outlet)
  if zone is not None:
    is_not_in_zone = ((fmerit.route[:,0]>=zone[0][0])*(fmerit.route[:,0]<=zone[1][0])*\
                      (fmerit.route[:,1]>=zone[1][1])*(fmerit.route[:,1]<=zone[0][1]))
    N = fmerit.route.shape[0] if np.all(is_not_in_zone) else np.argmax(~is_not_in_zone)
    return fmerit.route[:N,:].copy()
  else:
    return fmerit.route.copy()


def main_downstream(lon,lat,fdir,inlet,conv_db='trip'):
  '''
  Returns the main river path from inlet
  '''
  next_i,next_j = convention(conv_db)
  f = fdir.astype(int).copy()
  # Flip if increasing latitudes
  if lat[1]>lat[0]:
    lat = lat[::-1]
    f = f[::-1,:]
  # Get start point indices
  i = np.argmin(np.abs(lon-inlet[0]))
  j = np.argmin(np.abs(lat-inlet[1]))
  # Apply log2 for DRT convention
  if conv_db.lower()=='drt':
    #I = (f>0)*(f<=128)
    #f[I] = np.log2(f[I]).astype('u1')+1
    f = np.where((f>0)*(f<=128),np.log2(f)+1,0).astype(int)
  # Downstream route
  route = np.zeros((len(lon)*len(lat),2),'f4')
  route[0,:] = [lon[i],lat[j]]
  N = 1
  while f[j,i]>0 and f[j,i]<9:
    i,j = i+next_i[f[j,i]-1],j+next_j[f[j,i]-1]
    if i<0 or i>=len(lon) or j<0 or j>=len(lat): break
    route[N,:] = [lon[i],lat[j]]
    N = N+1
  return route[:N]


def main_downstream_trip(inlet,res=1./12):
  '''
  Returns the main river path from inlet using CTRIP network
  '''
  lon,lat = res2lonlat(res)
  fdir = trip_param('flowdir',res=res)
  route = main_downstream(lon,lat,fdir,inlet)
  return route


def main_downstream_merit(inlet,zone=None):
  '''
  Returns the main river path from inlet using the native MERIT dem
  '''
  fmerit.main_downstream(inlet)
  if zone is not None:
    N = np.argmax((fmerit.route[:,0]<zone[0][0])+(fmerit.route[:,0]>zone[1][0])+\
                  (fmerit.route[:,1]<zone[1][1])+(fmerit.route[:,1]>zone[0][1]))
    return fmerit.route[:N,:].copy()
  else:
    return fmerit.route.copy()


def mask2bnd(lon,lat,mask,method='direct',lvl=.5):
  '''
  Returns the boundary of a mask (two columns matrix: lon,lat)
  method can be:
    'direct'  : mask2bnd fortran routine
    'contourc': use of contourc
  '''
  if method=='direct':
    mask = np.array(mask,'u1')
    tmp = (mask>0)
    Ilon = slice(np.argmax(np.any(tmp,axis=0)),len(lon)-np.argmax(np.any(tmp,axis=0)[::-1]))
    Ilat = slice(np.argmax(np.any(tmp,axis=1)),len(lat)-np.argmax(np.any(tmp,axis=1)[::-1]))
    if Ilon.stop-Ilon.start==1:
      # Minimum 2 values on longitude to get the spatial resolution
      if Ilon.stop==len(lon): Ilon = slice(Ilon.start-1,Ilon.stop)
      else                  : Ilon = slice(Ilon.start,Ilon.stop+1)
    lon,lat,mask = lon[Ilon],lat[Ilat],mask[Ilat,Ilon]
    fbasin.mask2bnd(lon,lat,mask)
    bnd = fbasin.bnd
    bnd[bnd==-9999] = np.nan
  #elif method=='contourc':
  #  c = contourc(lon,lat,mask,[lvl])[0]
  #  for i in range(len(c)): c[i] = np.vstack((c[i],[[np.nan,np.nan]]))
  #  bnd = np.vstack(c)
  else:
    raise Exception('mask2bnd: unknown method')
  return bnd.copy()


def dir2acc(fdir,conv_db='trip',progress=True):
  '''
  Computes flow accumulation grid from flow direction grid
  '''
  f = fdir.copy()
  if conv_db.lower()=='drt':
    f = np.where((f>0)*(f<=128),np.log2(f)+1,0).astype(int)
  if False:
    next_i,next_j = convention(conv_db)
    nlat,nlon = f.shape
    facc = np.zeros((nlat,nlon),np.int64)
    mask = np.zeros((nlat,nlon),bool)
    def upstream_acc(j,i):
      if mask[j,i] or f[j,i]==0: return
      facc[j,i],mask[j,i] = 1,True
      for k in range(8):
        if ((j-next_j[k]<0)or(j-next_j[k]>=nlat)or(i-next_i[k]<0)or(i-next_i[k]>=nlon)): continue
        if f[j-next_j[k],i-next_i[k]]==k+1:
          upstream_acc(j-next_j[k],i-next_i[k])
          facc[j,i] = facc[j,i]+facc[j-next_j[k],i-next_i[k]]
      return
    tlat = trange(nlat) if progress else list(range(nlat))
    for j in tlat:
      for i in range(nlon):
        if not mask[j,i]:
          upstream_acc(j,i)
    return facc
  else:
    return fbasin.dir2acc(f,conv_db)


def river_network(lon,lat,fdir,facc,outlet,inlet=[],Nmax=None,Smin=None,extend_route=True,conv_db='trip'):
  '''
  Returns the river network upstream outlet
  '''
  nlon,nlat = len(lon),len(lat)
  next_i,next_j = convention(conv_db)
  RN_dict = {'id':[],'outlet':[],'area':[],'id_down':[],'route':np.zeros((0,2),'<f4'),
             'x_down':[],'hypso':np.zeros((0,2),'<f4'),'tributaries':[]}
  RN = []
  fd = fdir.copy()
  # Flip if increasing latitudes
  if lat[1]>lat[0]:
    lat = lat[::-1]
    fdir = fdir[::-1,:]
    facc = facc[::-1,:]
    flip_lat = True
  else:
    flip_lat = False
  # Get outlet indices
  iout = np.argmin(np.abs(lon-outlet[0]))
  jout = np.argmin(np.abs(lat-outlet[1]))
  # Get inlet indices
  stopin = np.zeros((nlat,nlon))
  for i in range(len(inlet)):
    iin = np.argmin(np.abs(lon-inlet[i,0]))
    jin = np.argmin(np.abs(lat-inlet[i,1]))
    stopin[jin,iin] = 1
  # First sub-basin
  RN.append(RN_dict.copy())
  RN[0]['id'] = 0
  RN[0]['outlet'] = [lon[iout],lat[jout]]
  RN[0]['area'] = facc[jout,iout]
  RN[0]['id_down'] = np.nan
  RN[0]['x_down'] = np.nan
  RN[0]['tributaries'] = {'id':[],'x':[],'S':[]}
  # Get minimum area
  if Smin is None: Smin = 0.01*facc[jout,iout]
  # Recursive call
  def stream_network(iRN,i,j):
    # Add point to route
    RN[iRN]['route'] = np.vstack((RN[iRN]['route'],[lon[i],lat[j]]))
    # Add hypsometry (distance is in number of pixels -> could be improved)
    if RN[iRN]['hypso'].shape[0] == 0: distance = 0
    else: distance = RN[iRN]['hypso'][-1,0]+1
    RN[iRN]['hypso'] = np.vstack((RN[iRN]['hypso'],[distance,facc[j,i]]))
    if stopin[j,i]: return
    # Build main upstream network
    Iup,Sup = [],0
    for k in range(8):
      if ((j-next_j[k]<0)or(j-next_j[k]>=nlat)or(i-next_i[k]<0)or(i-next_i[k]>=nlon)): continue
      f = fdir[j-next_j[k],i-next_i[k]]
      # Apply log2 for DRT convention
      if conv_db.lower()=='drt' and (f>0)*(f<=128): f = np.log2(f).astype('u1')+1
      if f==k+1:
        if facc[j-next_j[k],i-next_i[k]]>Sup:
          kup,Sup = k,facc[j-next_j[k],i-next_i[k]]
        Iup.append(k)
    if Sup<=Smin: return
    stream_network(iRN,i-next_i[kup],j-next_j[kup])
    # Build tributaries
    Iup.remove(kup)
    for k in Iup:
      inext,jnext = i-next_i[k],j-next_j[k]
      if facc[jnext,inext]<Smin: continue
      count = len(RN)
      RN.append(RN_dict.copy())
      RN[count]['id'] = count
      RN[count]['outlet'] = [lon[i],lat[j]]
      RN[count]['area'] = facc[jnext,inext]
      RN[count]['id_down'] = iRN
      RN[count]['x_down'] = distance
      RN[count]['route'] = np.array([lon[i],lat[j]])
      RN[count]['tributaries'] = {'id':[],'x':[],'S':[]}
      RN[iRN]['tributaries']['id'].append(count)
      RN[iRN]['tributaries']['x'].append(distance)
      RN[iRN]['tributaries']['S'].append(facc[jnext,inext])
      stream_network(count,inext,jnext)
  stream_network(0,iout,jout)
  # Sort bus-basins by area
  Isort = np.argsort([n['area'] for n in RN])[::-1]
  Nmax = min(Nmax,len(Isort)) if Nmax is not None else len(Isort)
  RNsort = [RN[0].copy()]
  for i in range(1,Nmax):
    RNsort.append(RN[Isort[i]])
    if extend_route:
      route = main_upstream(lon,lat,fdir,facc,RNsort[i]['route'][-1,:],conv_db=conv_db)
      RNsort[i]['route'] = np.vstack((RNsort[i]['route'],route))
    RNsort[i]['id'] = i
    RNsort[i]['id_down'] = Isort[RNsort[i]['id_down']]
    RNsort[i]['tributaries']['id'] = [Isort[j] for j in RNsort[i]['tributaries']['id']]
  return RNsort


def river_network_merit(main_outlet,inlet=[],Nmax=None,Smin=-1,extend_route=True):
  '''
  '''
  fmerit.river_network(main_outlet,inlet,Smin)
  RN_dict = {'id':[],'outlet':[],'area':[],'id_down':[],'route':np.zeros((0,2),'<f4'),
             'x_down':[],'hypso':np.zeros((0,2),'<f4'),'tributaries':[]}
  Isort = np.argsort(fmerit.rn_area)[::-1]
  Nmax = min(Nmax,len(Isort)) if Nmax is not None else len(Isort)
  RN = [RN_dict.copy() for i in range(Nmax)]
  Nroutesum = [0]+np.cumsum(fmerit.rn_Nroute).tolist()
  Ntribsum = [0]+np.cumsum(fmerit.rn_Ntrib).tolist()
  for iRN in range(Nmax):
    iRNsort = Isort[iRN]
    RN[iRN]['id'] = iRN
    RN[iRN]['outlet'] = fmerit.rn_outlet[iRNsort,:]
    RN[iRN]['area'] = fmerit.rn_area[iRNsort]
    RN[iRN]['id_down'] = Isort[fmerit.rn_id_down[iRNsort]-1] if fmerit.rn_id_down[iRNsort]!=-1 else np.nan
    RN[iRN]['x_down'] = fmerit.rn_x_down[iRNsort] if fmerit.rn_x_down[iRNsort]!=-1 else np.nan
    RN[iRN]['route'] = fmerit.rn_route[Nroutesum[iRNsort]:Nroutesum[iRNsort+1],:]
    if extend_route:
      route = main_upstream_merit(RN[iRN]['route'][-1,:])
      RN[iRN]['route'] = np.vstack((RN[iRN]['route'],route))
    RN[iRN]['hypso'] = fmerit.rn_hypso_xS[Nroutesum[iRNsort]:Nroutesum[iRNsort+1],:]
    RN[iRN]['tributaries'] = dict()
    RN[iRN]['tributaries']['id'] = Isort[fmerit.rn_trib_id[Ntribsum[iRNsort]:Ntribsum[iRNsort+1]]-1]
    RN[iRN]['tributaries']['xS'] = fmerit.rn_trib_xS[Ntribsum[iRNsort]:Ntribsum[iRNsort+1],:]
  return RN


def localize_station_merit(lon,lat,area,dist=5):
  '''
  Localize MERIT pixel with area closest to station area
  Automated repositioning algorithm from GRDC report #41 (Lehner, 2012)
  dist is in km
  Returns (x,y,distance,area,criterion)
  '''
  res = 1./1200
  coslat = np.cos(lat*np.pi/180)
  lat_dist = dist/earth_radius*180/np.pi
  lon_dist = lat_dist/coslat
  zone = [[np.floor((lon-lon_dist-res/2)/res)*res,np.ceil((lat+lat_dist+res/2)/res)*res],
          [lon+lon_dist,lat-lat_dist]]
  x,y = res2lonlat(res,zone=zone)
  X,Y = np.meshgrid(x,y)
  X,Y = X.flatten(),Y.flatten()
  # Distance criterion
  D = 111.*np.sqrt(((X-lon)*coslat)**2+(Y-lat)**2)
  RD = 100./dist*D
  # Area criterion
  A = merit_param('upa',zone).flatten()
  RA = 100*np.abs(A-area)/area
  # Find best score
  J = (RD<=100)*(RA<=50)
  if J.sum()==0:
    j = np.argmin(RD)
    X,Y,D,A,R = X[j],Y[j],D[j],A[j],np.nan
  else:
    X,Y,D,A = X[J],Y[J],D[J],A[J]
    R = RA[J]+RD[J]
    j = np.argmin(R)
    X,Y,D,A,R = X[j],Y[j],D[j],A[j],R[j]
  if   nargout()<=2: return (X,Y)
  else             : return (X,Y,D,A,R)[:nargout()]


def localize_station_trip(lon,lat,area,mask,zone,area_diff=0.2,mask_diff=0.4,px_dist=1,trip_pgd=None):
  '''
  Localize TRIP pixel
  Returns (x,y,area,mask,area_cost,mask_cost)
  area_cost = abs(trip_area-area)/area
  mask_cost = abs(common_mask_area-area)/area
  '''
  def area_cost(a):
    return np.abs(a-area)/area
  def masklr_cost(m):
    if False:
      cost = np.abs(spatial_area(trip_lon,trip_lat,m*mask)-area)/area
    else: # IoU
      cost = 1-spatial_area(trip_lon,trip_lat,m*mask)/spatial_area(trip_lon,trip_lat,np.minimum(m+mask,1))
    return cost
  if trip_pgd is None: trip_pgd = trip_pgd_dir+'/TRIP_PGD_12D.nc'
  with Dataset(trip_pgd) as nc:
    trip_lon,trip_lat = nc.variables['longitude'][:],nc.variables['latitude'][:]
    Ilon,Ilat = get_IlonIlat(trip_lon,trip_lat,zone)
    trip_lon,trip_lat = trip_lon[Ilon],trip_lat[Ilat]
    trip_area = 1e-6*nc.variables['DR_AREA'][Ilat,Ilon]
    trip_fdir = nc.variables['FLOWDIR'][Ilat,Ilon]
    if isinstance(trip_lon,np.ma.masked_array): trip_lon = trip_lon.filled(np.nan)
    if isinstance(trip_lat,np.ma.masked_array): trip_lat = trip_lat.filled(np.nan)
    if isinstance(trip_area,np.ma.masked_array): trip_area = trip_area.filled(np.nan)
    if isinstance(trip_fdir,np.ma.masked_array): trip_fdir = trip_fdir.filled(np.nan)
  ilon,ilat = np.argmin(np.abs(trip_lon-lon)),np.argmin(np.abs(trip_lat-lat))
  Ilon,Ilat = ilon+np.arange(-px_dist,px_dist+1),ilat+np.arange(-px_dist,px_dist+1)
  if False: # Priority to area
    acost = area_cost(trip_area[np.ix_(Ilat,Ilon)]).flatten()
    K = np.argsort(acost,axis=None)
    K = K[np.where(acost[K]<area_diff)[0]]
    for k in range(len(K)):
      ilon,ilat = Ilon[0]+np.mod(K[k],len(Ilat)),Ilat[0]+int(K[k]/len(Ilat))
      trip_mask = basin_mask(trip_lon,trip_lat,trip_fdir,[trip_lon[ilon],trip_lat[ilat]],conv_db='trip')
      mcost = masklr_cost(trip_mask)
      if trip_mask.sum()>1 and mcost<mask_diff: break
    acost = acost[k]
  else: # Area and mask
    acost,mcost = np.ones(len(Ilon)*len(Ilat)),np.ones(len(Ilon)*len(Ilat))
    trip_mask = np.empty(len(Ilon)*len(Ilat),object)
    for k in range(len(Ilon)*len(Ilat)):
      ilon,ilat = Ilon[0]+np.mod(k,len(Ilat)),Ilat[0]+int(k/len(Ilat))
      acost[k] = area_cost(trip_area[ilat,ilon])
      trip_mask[k] = basin_mask(trip_lon,trip_lat,trip_fdir,[trip_lon[ilon],trip_lat[ilat]],conv_db='trip')
      mcost[k] = masklr_cost(trip_mask[k])
    acost[acost>area_diff],mcost[mcost>mask_diff] = np.inf,np.inf
    #k = np.argmin(acost+2*mcost)
    k = np.argmin(mcost)
    ilon,ilat = Ilon[0]+np.mod(k,len(Ilat)),Ilat[0]+int(k/len(Ilat))
    acost,mcost,trip_mask = acost[k],mcost[k],trip_mask[k]
    K = [k] if np.isfinite(acost+mcost) else []
  if len(K)==0 or mcost>=mask_diff:
    #print('No TRIP point found for the station')
    trip_mask = np.zeros((len(trip_lat),len(trip_lon)),'u1')
    trip_lon,trip_lat,trip_area,trip_mask,acost,mcost = 6*(np.nan,)
  else:
    trip_lon,trip_lat,trip_area,trip_mask,acost,mcost = \
      trip_lon[ilon],trip_lat[ilat],trip_area[ilat,ilon],trip_mask,acost,mcost
  if   nargout()<=2: return (trip_lon,trip_lat)
  else             : return (trip_lon,trip_lat,trip_area,trip_mask,acost,mcost)[:nargout()]


def cart2pol(x, y):
  '''
  cartesian to polar coordinates
  '''
  theta = np.arctan2(y, x)
  rho = np.sqrt(x**2 + y**2)
  return (theta, rho)


def hillshade(dem,res,azimuth=70,altitude=45,zf=1):
  '''
  shaded relief using the ESRI algorithm
  '''
  # lighting azimuth
  azimuth = 360.0-azimuth+90 #convert to mathematic unit
  if azimuth>360 or azimuth==360:
    azimuth = azimuth-360
  azimuth = azimuth * (np.pi/180) #  convert to radians

  # lighting altitude
  altitude = (90-altitude) * (np.pi/180) # convert to zenith angle in radians

  # calc slope and aspect (radians)
  fx,fy = np.gradient(dem,res) # uses simple, unweighted gradient of immediate $
  [asp,grad] = cart2pol(fy,fx) # % convert to carthesian coordinates

  grad = np.arctan(zf*grad) #steepest slope
  # convert asp
  asp[asp<np.pi] = asp[asp<np.pi]+(np.pi/2)
  asp[asp<0] = asp[asp<0]+(2*np.pi)

  ## hillshade calculation
  h = 255.0*( (np.cos(altitude)*np.cos(grad) ) + ( np.sin(altitude)*np.sin(grad)*np.cos(azimuth - asp) ))
  h[h<0]=0 # % set hillshade values to min of 0.

  return h


def compute_river_length(lon,lat,fdir,conv_db='trip'):
  from math import radians, cos, sin, asin, sqrt
  f = fdir.copy()
  if conv_db.lower()=='drt':
    f = np.where((f>0)*(f<=128),np.log2(f)+1,0)
  next_i,next_j = convention(conv_db)
  nlat,nlon = f.shape
  leng = np.zeros((nlat,nlon),'f4')
  E2 = 0.006694470
  def calc_len(x1,y1,x2,y2):
    def calc_lon(y):
      yrad = y*np.pi/180
      return np.pi / 180.0 * earth_radius * np.cos(yrad) / np.sqrt(1.0 - E2 * np.sin(yrad) * np.sin(yrad))
    def calc_lat(y):
      yrad = y*np.pi/180
      return np.pi / 180.0 * earth_radius * (1.0-E2) / np.sqrt( (1.0 - E2 * np.sin(yrad) * np.sin(yrad))**3 )
    def calc_rad(y):
      yrad = y*np.pi/180
      rn = earth_radius / np.sqrt(1.0 - E2 *  np.sin(yrad) * np.sin(yrad) )
      return rn * np.sqrt( 1.0 - E2 * np.sin(yrad) + E2 * E2 * np.sin(yrad) * np.sin(yrad) )
    dx,dy = np.abs(np.mod(x1-x2+180,360)-180),np.abs(y1-y2)
    if x1==x2:
      l = calc_lat((y1+y2)/2)*dy
    elif y1==y2:
      l = calc_lon(y1)*dx
    else:
      y = (y1+y2)/2
      r = calc_rad(y)
      ddx = calc_lon(y)*dx/r
      ddy = calc_lat(y)*dy/r
      l = np.arccos(np.cos(ddx)*np.cos(ddy))*r
    return l
  def calc_len_my(x1,y1,x2,y2):
    x1,y1,x2,y2 = radians(x1),radians(y1),radians(x2),radians(y2)
    # Haversine formula
    dx,dy = x2-x1,y2-y1
    a = sin(dy / 2)**2 + cos(y1) * cos(y2) * sin(dx / 2)**2
    c = 2 * asin(sqrt(a))
    return(c * earth_radius)
  for j,y in tqdm(enumerate(lat)):
    for i,x in enumerate(lon):
      if fdir[j,i]>=1 and fdir[j,i]<=8:
        xn = lon[(i+next_i[fdir[j,i]-1])%len(lon)]
        yn = lat[j+next_j[fdir[j,i]-1]]
        leng[j,i] = 1000*calc_len(x,y,xn,yn)
      elif fdir[j,i]==9:
        leng[j,i] = 1000*calc_len(x,y,x,lat[j-1])
      else:
        leng[j,i] = 0
  return leng



