
from datetime import datetime as dt
import time as time
import numpy as np
import xarray as xr
import sys
import inspect
from scipy import optimize


earth_radius = 6371.

def nargout():
    '''
    Return how many values the caller is expecting
    Important: function call should not be splitted into several lines
    '''
    if sys.version_info[0]==3:
      import traceback
      callInfo = traceback.extract_stack()
      callLine = str(callInfo[-3].line)
      split_equal = callLine.split('(')[0].split('=')
      if len(split_equal)==1: return 0
      split_comma = split_equal[0].split(',')
      return len(split_comma)
    else:
      import dis
      f = inspect.currentframe()
      f = f.f_back.f_back
      c = f.f_code
      i = f.f_lasti
      bytecode = [b for b in c.co_code]
      instruction = bytecode[i+3]
      if instruction == dis.opmap['UNPACK_SEQUENCE']:
          howmany = bytecode[i+4]
          return howmany
      elif instruction == dis.opmap['PRINT_EXPR']:
          return 0
      return 1


def available_kwargs(f,kwargs):
    available_kwargs = inspect.getfullargspec(f).args
    return dict([(k,v) for k,v in kwargs.items() if k in available_kwargs])


def decyear(date_array):
  '''
  Return dates in decimal year format
  '''
  def sinceEpoch(date): # returns seconds since epoch
    return time.mktime(date.timetuple())
  decdate = np.zeros(len(date_array))
  for k,date in zip(list(range(len(date_array))),date_array):
    if isinstance(date,str): date = dt.strptime(date,'%Y%m%d')
    s = sinceEpoch
    year = date.year
    startOfThisYear = dt(year=year, month=1, day=1)
    startOfNextYear = dt(year=year+1, month=1, day=1)
    yearElapsed = s(date) - s(startOfThisYear)
    yearDuration = s(startOfNextYear) - s(startOfThisYear)
    fraction = yearElapsed/yearDuration
    decdate[k] = date.year + fraction
  return decdate


def leap_year(year, calendar='standard'):
  '''
  Determine if year is a leap year
  '''
  leap = False
  if ((calendar in ['standard', 'gregorian',
      'proleptic_gregorian', 'julian']) and
      (year % 4 == 0)):
      leap = True
      if ((calendar == 'proleptic_gregorian') and
          (year % 100 == 0) and
          (year % 400 != 0)):
          leap = False
      elif ((calendar in ['standard', 'gregorian']) and
                (year % 100 == 0) and (year % 400 != 0) and
                (year < 1583)):
          leap = False
  return leap


def get_dpm(time, calendar='standard'):
  '''
  Return a array of days per month corresponding to the months provided in `months`
  '''
  dpm = {'noleap': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        '365_day': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        'standard': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        'gregorian': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        'proleptic_gregorian': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        'all_leap': [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        '366_day': [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        '360_day': [0, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30]}
  month_length = np.zeros(len(time), dtype=np.int)
  cal_days = dpm[calendar]
  for i, (month, year) in enumerate(zip(time.month, time.year)):
      month_length[i] = cal_days[month]
      if leap_year(year, calendar=calendar):
          month_length[i] += 1
  return month_length


def progressbar(x=0,strlen=20):
  i = np.ceil(x*strlen);
  strprogress = '['+'-'*i+'>'+' '*(strlen-i)+'] {0:3d} %'.format(int(100*x));
  print(strprogress), #,end="\r");
  sys.stdout.flush();
  if x==1: print('\n')


def ran(x,*args,**kwargs):
  return np.vstack((np.nanmin(x,*args,**kwargs),np.nanmax(x,*args,**kwargs))).T


def res2lonlat(res,zone=None,latinvert=False):
  if zone is None: zone = [[-180,90],[180,-90]]
  res = float(res)
  lon = np.arange(zone[0][0]+res/2,zone[1][0],res)
  lat = np.arange(zone[0][1]-res/2,zone[1][1],-res)
  #lon = np.int64(np.round((lon+res/2)/res))*res-res/2
  #lat = np.int64(np.round((lat+res/2)/res))*res-res/2
  if latinvert: lat = lat[::-1]
  return (lon,lat,len(lon),len(lat))[:nargout()]


def get_IlonIlat(lon,lat,zone):
  zone = np.array(zone)
  point = False
  if zone.shape==(2,): point,zone = True,np.vstack((zone,zone))
  if point:
    Ilon = np.argmin(np.abs(lon-zone[0][0]))
    Ilat = np.argmin(np.abs(lat-zone[0][1]))
  else:
    Ilon = slice(np.argmax(lon>=zone[0][0]) if any(lon>=zone[0][0]) else len(lon),
                np.argmax(lon> zone[1][0]) if any(lon> zone[1][0]) else len(lon))
    if lat[0]>lat[1]: # decreasing lat
      Ilat = slice(np.argmax(lat<=zone[0][1]) if any(lat<=zone[0][1]) else len(lat),
                  np.argmax(lat< zone[1][1]) if any(lat< zone[1][1]) else len(lat))
    else:             # increasing lat
      Ilat = slice(np.argmax(lat>=zone[1][1]) if any(lat>=zone[1][1]) else len(lat),
                  np.argmax(lat> zone[0][1]) if any(lat> zone[0][1]) else len(lat))
    #if point: Ilon,Ilat = Ilon.start,Ilat.start
  return Ilon,Ilat


def convention(db='trip',latinvert=False):
  if str.lower(db)=='trip':
    # TRIP
    next_i = [0,1,1,1,0,-1,-1,-1]
    if not latinvert:
      next_j = [-1,-1,0,1,1,1,0,-1] # decreasing latitude
    else:
      next_j = [1,1,0,-1,-1,-1,0,1] # increasing latitude
  elif str.lower(db)=='drt':
    # DRT
    next_i = [1,1,0,-1,-1,-1,0,1]
    next_j = [0,1,1,1,0,-1,-1,-1] # decreasing latitude
  return np.array(next_i),np.array(next_j)


def ynormal(Q,W,m,S,n):
  '''
  Compute normal depth for a trapezoidal channel
  '''
  def cost(y):
    yp = np.abs(y)
    A,P = yp*(W+m*yp),W+2*yp*np.sqrt(1+m**2)
    return np.abs(n*(P**2/A**5)**(1/3)*Q-np.sqrt(S))
  y0 = (n*Q/W/np.sqrt(S))**(3/5)
  return optimize.minimize(cost,y0).x[0]


def drt2trip(fdir):
  fdir_trip = np.where((fdir>0)*(fdir<=128),np.mod(np.log2(fdir)+2,8)+1,
                       np.where(fdir==247,0,9)).astype(int)
  return fdir_trip


def var_resample(v,n,rdim=None,MV=None,pmin=0.5,method='mean'):
  '''
  Proceed to the resampling of the N-D variable v
  Resampling dimensions are given by rdim, while n is the window size
  If rdim is not given, all dimensions are resampled
  If shape of v is [M,N,P] and rdim=[0,1], resulting shape would be [M/n,N/n,P]
  Resampled dimensions have to be multiples of n
  pmin is the minimum fraction of available points required to compute the mean
  '''
  DIMS = v.shape
  if rdim is None: rdim = list(range(len(DIMS)))
  if any([np.mod(DIMS[r],n)>0 for r in rdim]):
    raise Exception('\nError in var_resample: Resampled dimensions have to be multiples of n')
  # Reshape
  NEW_DIMS = DIMS[:rdim[0]]+(int(DIMS[rdim[0]]/n),n)
  I_DIMS = (rdim[0]+1,)
  for i in range(1,len(rdim)):
    if rdim[i] > rdim[i-1]+1:
      NEW_DIMS = NEW_DIMS+DIMS[rdim[i-1]+1:rdim[i]]+(int(DIMS[rdim[i]]/n),n)
    else: NEW_DIMS = NEW_DIMS+(int(DIMS[rdim[i]]/n),n)
    I_DIMS = I_DIMS+(I_DIMS[i-1]+rdim[i]-rdim[i-1]+1,)
  if rdim[-1] < len(DIMS)-1: NEW_DIMS = NEW_DIMS+(DIMS[rdim[-1]+1:])
  J_DIMS = list(I_DIMS)+[i for i in range(len(NEW_DIMS)) if i not in I_DIMS]
  TR_DIMS = [n**len(rdim)]+[NEW_DIMS[i] for i in range(len(NEW_DIMS)) if i not in I_DIMS]
  # Missing value
  if MV is None: MV = np.nan if not 'int' in v.dtype.name else 999
  if np.isnan(MV): v_masked = np.ma.masked_array(v,np.isnan(v),dtype=v.dtype)
  else: v_masked = np.ma.masked_array(v,v==MV,dtype=v.dtype)
  v_new = np.transpose(v_masked.reshape(NEW_DIMS),J_DIMS).reshape(TR_DIMS)
  # Minimum fraction of available data
  Nmin = np.where(np.sum(v_new.mask,axis=0)>int((1-pmin)*n*n),True,False)
  # Compute the mean over each resampling dimension
  def most_frequent(v,axis=0):
    from scipy.stats import mode
    v_new = np.squeeze(mode(v,axis=axis).mode)
    if np.isnan(MV): v_new = np.ma.masked_array(v_new,np.isnan(v_new),dtype=v_new.dtype)
    else: v_new = np.ma.masked_array(v_new,v_new==MV,dtype=v_new.dtype)
    return v_new
  if   method=='mean'   : ftrans = np.mean
  elif method=='max'    : ftrans = np.max
  elif method=='median' : ftrans = np.median
  elif method=='sum'    : ftrans = np.sum
  elif method=='freqmax': ftrans = most_frequent
  v_new = ftrans(v_new,axis=0)
  v_new.mask = v_new.mask+Nmin
  return v_new.filled(fill_value=MV).astype(v.dtype)


def moving_average(a,n=3,axis=0):
  '''
  Moving average
  '''
  a = np.array(a)
  shp = a.shape
  Naxis = len(shp)
  axis = np.mod(axis,Naxis)
  I = [axis]+list(range(axis))+list(range(axis+1,Naxis))
  b = np.transpose(a,I)
  c = np.nan*np.ones(b.shape)
  for k in range(n,shp[axis]-n):
    c[k,...] = np.nanmean(b[k-n:k+n+1,...],axis=0)
  c = np.transpose(c,[I.index(i) for i in range(Naxis)])
  return c


def filter_nan(s,o):
  '''
  Removes NaN data from simulated and observed
  '''
  data = np.array([s.flatten(),o.flatten()])
  data = np.transpose(data)
  data = data[(~np.isnan(data)).all(1)]
  #data = data[~np.isnan(data)]
  return data[:,0],data[:,1]


def NSE(s,o,log=False):
  '''
  Nash Sutcliffe Efficiency coefficient
  '''
  s,o = filter_nan(s,o)
  #if len(s)==0 or s.min()==s.max(): return np.nan
  if len(s)==0: return np.nan
  if log:
    I = (s>0)*(o>0)
    s,o = s[I],o[I]
    if len(s)==0: return np.nan
    return 1 - sum((np.log(s)-np.log(o))**2)/sum((np.log(o)-np.mean(np.log(o)))**2)
  else:
    return 1 - np.sum((s-o)**2)/np.sum((o-np.mean(o))**2)


def KGE(s,o,version=2012):
  '''
  Kling-Gupta Efficiency
  version is 2009 or 2012
  '''
  s,o = filter_nan(s,o)
  if len(s)==0 or s.min()==s.max():
    return np.array((np.nan,np.nan,np.nan,np.nan))[:nargout()]
  r = np.corrcoef(s,o)[0,1]
  alpha = np.std(s)/np.std(o)
  beta  = np.mean(s)/np.mean(o)
  gamma = alpha/beta
  if version==2009:
    kge = 1-np.sqrt((r-1)**2+(alpha-1)**2+(beta-1)**2)
    out = (kge,r,alpha,beta)
  elif version==2012:
    kge = 1-np.sqrt((r-1)**2+(gamma-1)**2+(beta-1)**2)
    out = (kge,r,gamma,beta)
  return np.array(out[:nargout()])


def rmse(s,o):
  '''
  Root Mean Squared Error
  '''
  s,o = filter_nan(s,o)
  if len(s)==0: return np.nan
  return np.sqrt(np.mean((s-o)**2))


def mre(s,o):
  '''
  Mean Relative Error
  '''
  s,o = filter_nan(s,o)
  if len(s)==0: return np.nan
  return np.mean(np.abs(s-o)/o)


def correlation(s,o):
  '''
  correlation coefficient
  '''
  s,o = filter_nan(s,o)
  if len(s)==0: return np.nan
  return np.corrcoef(o,s)[0,1]


def ratio(s,o):
  '''
  ratio simulated / observed
  '''
  s,o = filter_nan(s,o)
  if len(s)==0: return np.nan
  return np.mean(s)/np.mean(o)
  #return np.mean(s/o)


def mk_test(t, x):
  '''
  Mann-Kendall test
  '''
  from scipy.stats import norm
  # remove nan or negative values
  #a = np.array(range(len(x)))+1.
  a = t
  w = np.where(x!=999)
  a = a[w]
  x = x[w]
  n = len(x)
  # calculate S 
  s = 0
  for k in range(n-1):
      for j in range(k+1,n):
          s += np.sign(x[j] - x[k])
  # calculate the unique data
  unique_x = np.unique(x)
  g = len(unique_x)
  # calculate the var(s)
  if n == g: # there is no tie
      var_s = (n*(n-1)*(2*n+5))/18
  else: # there are some ties in data
      tp = np.zeros(unique_x.shape)
      for i in range(len(unique_x)):
          tp[i] = sum(unique_x[i] == x)
      var_s = (n*(n-1)*(2*n+5) - np.sum(tp*(tp-1)*(2*tp+5)))/18
  if s>0:
      z = (s - 1)/np.sqrt(var_s)
  elif s == 0:
      z = 0
  elif s<0:
      z = (s + 1)/np.sqrt(var_s)
  # calculate the p_value
  p = 2*(1-norm.cdf(abs(z))) # two tail test
  # calculate the slope
  slp_val_array = []
  for k in range(n-1):
      for j in range(k+1,n):
          Slope = (x[k] - x[j])/(a[k]-a[j])
          slp_val_array.append(Slope)
  Sn = np.median(slp_val_array)
  return p, z, Sn, len(x) #,min(x),max(x),a


def earth_distance(lon1,lat1,lon2,lat2):
  '''
  Distance (in km) between two points on Earth)
  '''
  from math import radians, cos, sin, asin, sqrt 
  lon1,lat1,lon2,lat2 = radians(lon1),radians(lat1),radians(lon2),radians(lat2)
  # Haversine formula
  dlon,dlat = lon2-lon1,lat2-lat1
  a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
  c = 2 * asin(sqrt(a))
  return(c * earth_radius)


def spatial_area(lon,lat,mask=None):
  '''
  Spatial area (km2)
  works with uniform grid
  '''
  if mask is None: mask = np.ones((len(lat),len(lon)),'f4')
  if not isinstance(lon,np.ndarray): lon,lat = np.array(lon),np.array(lat)
  dlon,dlat = lon[1]-lon[0],np.abs(lat[0]-lat[1])
  Ilon = (np.sum(mask,axis=0)>0)
  Ilat = (np.sum(mask,axis=1)>0)
  _,Y = np.meshgrid(lon[Ilon],lat[Ilat])
  pond = mask[np.ix_(Ilat,Ilon)]*np.cos(Y*np.pi/180.)
  A = earth_radius**2*dlat*np.pi/180.*dlon*np.pi/180.*np.sum(pond)
  return A


def spatial_mean(lon,lat,IN,mask=None,stdout=False):
  '''
  Spatial mean
  '''
  if len(IN.shape)==3:
    ilon = [i for i,s in enumerate(IN.shape) if s==len(lon)][0]
    ilat = [i for i,s in enumerate(IN.shape) if s==len(lat)][0]
    idat = [i for i in range(3) if not i in [ilon,ilat]][0]
    IN = IN.transpose((ilat,ilon,idat))
    ndat = IN.shape[2]
  else:
    ndat = 1
    IN = IN.reshape(IN.shape+(1,))
  if type(IN) == np.ma.core.MaskedArray:
    mask = 1-np.all(IN.mask.astype('int8'),axis=2)
    IN = IN.filled(np.nan)
  if mask is None: mask = np.ones((IN.shape[0],IN.shape[1]))
  Ilon = (np.sum(mask,axis=0)>0)
  Ilat = (np.sum(mask,axis=1)>0)
  _,Y = np.meshgrid(lon[Ilon],lat[Ilat])
  pond = mask[np.ix_(Ilat,Ilon)]*np.cos(Y*np.pi/180.)
  # spatial mean
  OUT = np.nan*np.ones((ndat,))
  for k in range(ndat):
    tmp = IN[:,:,k][np.ix_(Ilat,Ilon)]
    Inan = ~np.isnan(tmp)
    if np.sum(Inan)==0: continue
    if pond.shape[0] > 1: OUT[k] = np.matmul(pond[Inan].T,tmp[Inan])/np.sum(pond[Inan])
    else                : OUT[k] = np.matmul(pond[Inan],tmp[Inan].T)/np.sum(pond[Inan])
  # spatial standard deviation
  if stdout:
    STD = np.nan*np.ones((ndat,))
    for k in range(ndat):
      tmp = (IN[:,:,k][np.ix_(Ilat,Ilon)]-OUT[k])**2
      Inan = ~np.isnan(tmp)
      if np.sum(Inan)==0: continue
      M = np.sum(pond[Inan]!=0)
      M = (M-1.)/M
      if pond.shape[0] > 1: STD[k] = np.sqrt(np.matmul(pond[Inan].T,tmp[Inan])/(M*np.sum(pond[Inan])))
      else                : STD[k] = np.sqrt(np.matmul(pond[Inan],tmp[Inan].T)/(M*np.sum(pond[Inan])))
  # output
  if ndat==1:
    OUT = OUT[0]
    if stdout: STD = STD[0]
  if not stdout:
    return OUT
  else:
    return OUT,STD


def seasonal_mean(*args,**kwargs):
  '''
  Compute seasonal mean
  season is 'month' or 'season' ('DJF','MAM','JJA','SON')
  Examples:
    climatology_mean = seasonal_mean(dat,data) # data.shape[0]=len(dat)
    climatology_mean = seasonal_mean(ds) # ds being a xarray.Dataset
    climatology_mean,climatology_std = seasonal_mean(ds) # ds being a xarray.Dataset
  '''
  if len(args)==2:
    ds = xr.DataArray(args[1],coords=[args[0]]+[np.arange(i) for i in args[1].shape[1:]],
                      dims=['time']+['dim{:d}'.format(i) for i in range(len(args[1].shape)-1)])
  else:
    ds = args[0]
  if not isinstance(ds,xr.DataArray):
    raise Exception('Error seasonal_mean: ds must be xarray.Dataset')
  season = kwargs['season'] if 'season' in kwargs else 'month'
  calendar = kwargs['calendar'] if 'calendar' in kwargs else 'standard'
  ## Make a DataArray with the number of days in each month, size = len(time)
  #month_length = xr.DataArray(get_dpm(ds.time.to_index(), calendar=calendar),
                              #coords=[ds.time], name='month_length')
  ## Calculate the weights by grouping by 'time.month' or 'time.season'
  #weights = month_length.groupby('time.'+season) / month_length.astype(float).groupby('time.'+season).sum()
  ## Test that the sum of the weights for each month/season is 1.0
  #np.testing.assert_allclose(weights.groupby('time.'+season).sum().values,np.ones(4 if season=='season' else 12))
  ## Calculate the weighted average
  #climatology_mean = (ds * weights).groupby('time.'+season).sum(dim='time')
  climatology_mean = ds.groupby('time.'+season).mean('time')
  if len(args)==2: climatology_mean = climatology_mean.values
  # Compute standard deviation
  climatology_std = None
  if nargout()==2:
    climatology_std = ds.groupby('time.'+season).std('time')
    if len(args)==2: climatology_std = climatology_std.values
  # Output
  return (climatology_mean,climatology_std)[:nargout()]


def read_bil(filename):
  '''
  Read bil/bsq/dir file with associated header file (hdr)
  '''
  from pandas import read_csv
  hdrfile = filename.replace('.bil','.hdr').replace('.bsq','.hdr').replace('.dir','.hdr')
  l = read_csv(hdrfile,sep=' ',index_col=0,skipinitialspace=True,header=None).to_dict()[1]
  l = {k.upper():v for k,v in l.items()}
  lon = np.arange(float(l['ULXMAP']),float(l['ULXMAP'])+int(l['NCOLS'])*float(l['XDIM']),float(l['XDIM']))
  lat = np.arange(float(l['ULYMAP']),float(l['ULYMAP'])-int(l['NROWS'])*float(l['YDIM']),-float(l['YDIM']))
  lon,lat = lon[:int(l['NCOLS'])],lat[:int(l['NROWS'])]
  if l['NBITS']=='1':
    data = np.unpackbits(np.fromfile(filename,'u1'))[:len(lat)*len(lon)].reshape((len(lat),len(lon)))
  else:
    byteorder = '>' if 'BYTEORDER' in l and l['BYTEORDER']=='M' else '='
    pixeltype = 'u'
    if 'PIXELTYPE' in l:
      if l['PIXELTYPE'].upper()=='FLOAT': pixeltype = 'f'
      elif l['PIXELTYPE'].upper()=='SIGNEDINT': pixeltype = 'i'
    nbyte = str(int(l['NBITS'])//8)
    dtype = byteorder+pixeltype+nbyte
    data = np.fromfile(filename,dtype).reshape((len(lat),len(lon)))
  return lon,lat,data


def write_bil(filename,x,y,z,dtype=None,nodata=None):
  '''
  Write bil file and associated header file (hdr)
  '''
  if y[1]>y[0]:
    y = y[::-1]
    z = z[::-1,:]
  if dtype is None: dtype = z.dtype
  d = np.zeros(0,dtype).dtype
  nbits = 1 if 'b' in d.str else 8*d.itemsize
  pixeltype = 'FLOAT' if d.kind=='f' else 'SIGNEDINT' if d.kind=='i' else 'UNSIGNEDINT'
  hdrfile = filename.replace('.bil','.hdr').replace('.bsq','.hdr')
  with open(hdrfile,'w') as fid:
    fid.write('LAYOUT          {:s}\n'.format(filename[-3:].upper()))
    fid.write('BYTEORDER       {:s}\n'.format('M' if d.byteorder=='>' else 'I'))
    fid.write('NBITS           {:d}\n'.format(nbits))
    fid.write('PIXELTYPE       {:s}\n'.format(pixeltype))
    fid.write('NROWS           {:d}\n'.format(len(y)))
    fid.write('NCOLS           {:d}\n'.format(len(x)))
    fid.write('NBANDS          1\n')
    fid.write('ULXMAP          {:.16f}\n'.format(x[0]))
    fid.write('ULYMAP          {:.16f}\n'.format(y[0]))
    fid.write('XDIM            {:.16f}\n'.format(x[1]-x[0]))
    fid.write('YDIM            {:.16f}\n'.format(y[0]-y[1]))
    if nodata is not None: fid.write('NODATA          {:d}\n'.format(nodata))
  if nbits==1: z = np.packbits(z.astype('u1'))
  else       : z = z.astype(dtype)
  z.tofile(filename)
  return




