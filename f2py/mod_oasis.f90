module oasis

contains

subroutine oasis_remap_field(nrmp,nin,ndat,src,dst,rmp,fieldin,nout,fieldout)

implicit none

character(len=255), parameter :: remap_file = '/home/muniers/src/Python/myPyFunc/data/remap_SAFRAN_to_CTRIP.nc'

! Input variables
integer,                    intent(in)  :: nrmp        ! number of remap lines
integer,                    intent(in)  :: nin         ! number of input lines
integer,                    intent(in)  :: nout        ! number of output lines
integer,                    intent(in)  :: ndat        ! number of dates
real, dimension(nrmp),      intent(in)  :: src
real, dimension(nrmp),      intent(in)  :: dst
real, dimension(nrmp),      intent(in)  :: rmp
real, dimension(ndat,nin),  intent(in)  :: fieldin
real, dimension(ndat,nout), intent(out) :: fieldout

! Output variables

! Local variables
integer :: i,k

fieldout(:,:) = 0.
do i = 1,nrmp
!   do k = 1,ndat
    fieldout(:,dst(i)) = fieldout(:,dst(i))+fieldin(:,src(i))*rmp(i)
!   enddo
enddo

end subroutine oasis_remap_field

end module oasis
