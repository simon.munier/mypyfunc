module basin

! Types
type int4_type
  integer  , allocatable, dimension(:,:) :: data
end type int4_type

! Parameters
integer  , parameter                   :: Nmax = 10000000

! Variables
real*8   , allocatable, dimension(:,:) :: bnd   ! basin boundary (lon,lat)
! real*8   , allocatable, dimension(:,:) :: route ! up/down-stream route (lon,lat)


contains


!=================================================================
!
!  convention
!
!=================================================================
subroutine convention(conv_db,next_i,next_j)

implicit none

! Input variables
character(len=4), intent(in) :: conv_db

! Output variables
integer, dimension(1:8), intent(out) :: next_i,next_j

if (trim(conv_db)=='trip') then
  next_i = (/0,1,1,1,0,-1,-1,-1/)
  next_j = (/-1,-1,0,1,1,1,0,-1/) ! decreasing latitude
else
  next_i = (/1,1,0,-1,-1,-1,0,1/)
  next_j = (/0,1,1,1,0,-1,-1,-1/) ! decreasing latitude
endif

end subroutine convention


!=================================================================
!
!  basin_mask
!
!=================================================================
subroutine basin_mask(nlon,nlat,fdir,outlet,conv_db,mask)

implicit none

! Input variables
integer, intent(in)  :: nlon                    ! number of longitudes
integer, intent(in)  :: nlat                    ! number of latitudes
integer, intent(in)  :: fdir(0:nlat-1,0:nlon-1) ! flow direction matrix (must be decreasing latitude)
integer, intent(in)  :: outlet(0:1)             ! coordinate indices (i,j) of the outlet (starting at 0)
character(len=4), intent(in) :: conv_db         ! flow direction convention

! Output variables
integer*1, intent(out) :: mask(0:nlat-1,0:nlon-1) ! basin mask

! Local variables
integer, dimension(1:8) :: next_i,next_j

call convention(conv_db,next_i,next_j)

mask(:,:) = 0
call previous_ij(outlet(0),outlet(1))


contains

  recursive subroutine previous_ij(i,j)
    integer :: i,j,k
    mask(j,i) = 1
    do k = 1,8
      if ( (j-next_j(k)<0) .or. (j-next_j(k)>=nlat) .or. &
           (i-next_i(k)<0) .or. (i-next_i(k)>=nlon) ) cycle
      if (fdir(j-next_j(k),i-next_i(k)) == k) call previous_ij(i-next_i(k),j-next_j(k))
    enddo
  end subroutine previous_ij

end subroutine basin_mask


!=================================================================
!
!  main_upstream
!
!=================================================================
subroutine main_upstream(nlon,nlat,fdir,facc,outlet,conv_db,Nroute,route)

implicit none

! Input variables
integer, intent(in)  :: nlon                    ! number of longitudes
integer, intent(in)  :: nlat                    ! number of latitudes
integer, intent(in)  :: fdir(0:nlat-1,0:nlon-1) ! flow direction matrix (must be decreasing latitude)
real*4 , intent(in)  :: facc(0:nlat-1,0:nlon-1) ! flow direction matrix (must be decreasing latitude)
integer, intent(in)  :: outlet(0:1)             ! coordinate indices (i,j) of the outlet (starting at 0)
character(len=4), intent(in) :: conv_db         ! flow direction convention

! Output variables
integer, intent(out) :: Nroute                  ! number of vertices in the route
integer, intent(out) :: route(1:nlon*nlat,1:2) ! upstream route

! Local variables
integer, dimension(1:8) :: next_i,next_j

call convention(conv_db,next_i,next_j)

Nroute = 0
call previous_ij(outlet(0),outlet(1))


contains

  recursive subroutine previous_ij(i,j)
    integer :: i,j,k,kup
    real*4  :: Sup
    Nroute = Nroute+1
    route(Nroute,:) = (/i,j/)
    Sup = 0
    do k = 1,8
      if ( (j-next_j(k)<0) .or. (j-next_j(k)>=nlat) .or. &
           (i-next_i(k)<0) .or. (i-next_i(k)>=nlon) ) cycle
      if (fdir(j-next_j(k),i-next_i(k)) == k .and. facc(j-next_j(k),i-next_i(k))>Sup) then
        kup = k
        Sup = facc(j-next_j(k),i-next_i(k))
      endif
    enddo
    if (Sup>0) call previous_ij(i-next_i(kup),j-next_j(kup))
  end subroutine previous_ij

end subroutine main_upstream


!=================================================================
!
!  river_outlet
!
!=================================================================
subroutine river_outlet(nlon,nlat,fdir,inlet,conv_db,outlet)

implicit none

! Input variables
integer, intent(in)  :: nlon                    ! number of longitudes
integer, intent(in)  :: nlat                    ! number of latitudes
integer, intent(in)  :: fdir(0:nlat-1,0:nlon-1) ! flow direction matrix (must be decreasing latitude)
integer, intent(in)  :: inlet(0:1)              ! coordinate indices (i,j) of the inlet (starting at 0)
character(len=4), intent(in) :: conv_db         ! flow direction convention

! Output variables
integer, intent(out) :: outlet(0:1)             ! coordinate indices (i,j) of the outlet (starting at 0)

! Local variables
integer :: i,j
integer, dimension(1:8) :: next_i,next_j

call convention(conv_db,next_i,next_j)

i = inlet(0)
j = inlet(1)

call next_ij(j,i)

outlet = (/i,j/)


contains

  recursive subroutine next_ij(j,i)
    integer, intent(inout) :: i,j
    if (fdir(j,i)>0 .and. fdir(j,i)<9) then
      i = i+next_i(fdir(j,i))
      j = j+next_j(fdir(j,i))
      call next_ij(j,i)
    endif
  end subroutine next_ij

end subroutine river_outlet


!=================================================================
!
!  dir2acc
!
!=================================================================
subroutine dir2acc(nlon,nlat,fdir,conv_db,facc)

implicit none

! Input variables
integer, intent(in)  :: nlon                    ! number of longitudes
integer, intent(in)  :: nlat                    ! number of latitudes
integer, intent(in)  :: fdir(0:nlat-1,0:nlon-1) ! flow direction matrix (must be decreasing latitude)
character(len=4), intent(in) :: conv_db         ! flow direction convention

! Output variables
integer, intent(out) :: facc(0:nlat-1,0:nlon-1) ! coordinate indices (i,j) of the outlet (starting at 0)

! Local variables
integer :: i,j
integer, dimension(1:8) :: next_i,next_j
logical :: mask(0:nlat-1,0:nlon-1)

call convention(conv_db,next_i,next_j)

facc(:,:) = 0
mask(:,:) = .false.

do i = 0,nlon-1
  do j = 0,nlat-1
    if (.not. mask(j,i)) call upstream_acc(j,i)
  enddo
enddo

contains

  recursive subroutine upstream_acc(j,i)
    integer, intent(in) :: i,j
    integer :: pi,pj,k
    if (mask(j,i) .or. fdir(j,i)==0) return
    facc(j,i) = 1
    mask(j,i) = .true.
    do k = 1,8
      pi = i-next_i(k)
      pj = j-next_j(k)
      if (pj<0 .or. pj>=nlat) cycle
      if (pi<0) pi = pi+nlon
      if (pi>=nlon) pi = pi-nlon
      !if ( (j-next_j(k)<0) .or. (j-next_j(k)>=nlat) .or. &
      !     (i-next_i(k)<0) .or. (i-next_i(k)>=nlon) ) cycle
      if (fdir(pj,pi) == k) then
        call upstream_acc(pj,pi)
        facc(j,i) = facc(j,i)+facc(pj,pi)
      endif
    enddo
  end subroutine upstream_acc

end subroutine dir2acc


!=================================================================
!
!  mask2bnd
!
!=================================================================
subroutine mask2bnd(nlon,nlat,lon,lat,mask)

implicit none

! Input variables
integer  , intent(in)  :: nlon                     ! number of longitudes
integer  , intent(in)  :: nlat                     ! number of latitudes
real*8   , intent(in)  :: lon(0:nlon-1)            ! vector of longitudes
real*8   , intent(in)  :: lat(0:nlat-1)            ! vector of latitudes
integer*1, intent(in)  :: mask(0:nlat-1,0:nlon-1)  ! mask

! Local variables
real*8 :: lon_ext(0:nlon),lat_ext(0:nlat),res
type(int4_type), dimension(1:1000) :: bnd_points
integer*1 :: mask_new(0:nlat-1,0:nlon-1)
integer :: i,j,k,i0,j0,d,go_next,Nmask,Nbnd,Nbnd_points
integer :: imin,imax,jmin,jmax
integer, dimension(0:3) :: np1,np2,nd1,nd2
integer :: Q(0:nlon*nlat-1,1:2),NQ
integer*1 :: m

if (allocated(bnd)) deallocate(bnd)

np1 = (/0,0,-1,-1/)
np2 = (/-1,0,0,-1/)
nd1 = (/1,0,-1,0/)
nd2 = (/0,1,0,-1/)

res = lon(1)-lon(0)
lon_ext(0:nlon-1) = lon(:)
lon_ext(nlon) = lon(nlon-1)+res
if (lat(1)>lat(0)) then ! increasing latitudes
  lat_ext(0:nlat-1) = lat(nlat-1:0:-1)
  lat_ext(nlat) = lat(0)-res
else                    ! decreasing latitudes
  lat_ext(0:nlat-1) = lat(:)
  lat_ext(nlat) = lat(nlat-1)-res
endif

Nmask = 0
do i = 0,nlon-1
  do j = 0,nlat-1
    if (lat(1)>lat(0)) then
      m = mask(nlat-1-j,i)
    else
      m = mask(j,i)
    endif
    if (m>0) then
      mask_new(j,i) = 1
      Nmask = Nmask+1
    else
      mask_new(j,i) = 0
    endif
  enddo
enddo

Nbnd_points = 1
allocate(bnd_points(1)%data(1:Nmax,1:2))

Nbnd = 0
do while (Nmask>0)

  ! Find start point
  do j0 = 0,nlat-1
    do i0 = 0,nlon-1
      if (mask_new(j0,i0)==1) exit
    enddo
    if (i0<nlon .and. mask_new(j0,i0)==1) exit
  enddo
  if (j0>0) mask_new(j0-1,i0-1:i0) = -1
  i = i0+1
  j = j0

  imin = max(i-1,0)
  imax = min(i,nlon-1)
  jmin = max(j,0)
  jmax = min(j,nlat-1)

  call add_to_bnd(i-1,j)
  call add_to_bnd(i,j)

  ! Find contour
  d = 0
  do while (bnd_points(Nbnd_points)%data(Nbnd,1)/=i0 .or. bnd_points(Nbnd_points)%data(Nbnd,2)/=j0)
    go_next = 0
    do while (go_next==0)
      do while (i+np1(d)<0.or.i+np1(d)>=nlon.or.j+np2(d)<0.or.j+np2(d)>=nlat)
        d = modulo(d+1,4)
      enddo
      if (mask_new(j+np2(d),i+np1(d)) == 1) then
        d = modulo(d-1,4)
        go_next = 1
      else
        mask_new(j+np2(d),i+np1(d)) = -1
        d = modulo(d+1,4)
      endif
    enddo
    i = i+nd1(d)
    j = j+nd2(d)
    if (i<imin) imin = max(i,0)
    if (i>imax) imax = min(i,nlon-1)
    if (j<jmin) jmin = max(j,0)
    if (j>jmax) jmax = min(j,nlat-1)
    call add_to_bnd(i,j)
  enddo

  call update_mask(j0,i0)

  call add_to_bnd(-1,-1)

enddo


allocate(bnd(1:Nbnd+(Nbnd_points-1)*Nmax,1:2))
do j = 1,Nbnd_points
  k = Nmax
  if (j==Nbnd_points) k = Nbnd
  do i = 1,k
    if (bnd_points(j)%data(i,1)==-1) then
      bnd(i+(j-1)*Nmax,:) = -9999.
    else
      bnd(i+(j-1)*Nmax,1) = lon_ext(bnd_points(j)%data(i,1))-res/2
      bnd(i+(j-1)*Nmax,2) = lat_ext(bnd_points(j)%data(i,2))+res/2
    endif
  enddo
  deallocate(bnd_points(j)%data)
enddo


contains

  subroutine add_to_bnd(i,j)
    integer, intent(in) :: i,j
    Nbnd = Nbnd+1
    if (Nbnd>Nmax) then
      Nbnd_points = Nbnd_points+1
      allocate(bnd_points(Nbnd_points)%data(1:Nmax,1:2))
      Nbnd = 1
    endif
    bnd_points(Nbnd_points)%data(Nbnd,:) = (/i,j/)
  end subroutine

  subroutine update_mask(j1,i1)
    integer, intent(in) :: i1,j1
    integer :: i,j
    NQ = 0
    call add_to_q(j1,i1)
    do while (NQ>0)
      j = Q(NQ,1)
      i = Q(NQ,2)
      NQ = NQ-1
      if (i>imin.and.j>jmin.and.(mask_new(j-1,i-1)==0.or.mask_new(j-1,i-1)==1)) call add_to_q(j-1,i-1)
      if (i>imin           .and.(mask_new(j  ,i-1)==0.or.mask_new(j  ,i-1)==1)) call add_to_q(j  ,i-1)
      if (i>imin.and.j<jmax.and.(mask_new(j+1,i-1)==0.or.mask_new(j+1,i-1)==1)) call add_to_q(j+1,i-1)
      if (           j>jmin.and.(mask_new(j-1,i  )==0.or.mask_new(j-1,i  )==1)) call add_to_q(j-1,i  )
      if (           j<jmax.and.(mask_new(j+1,i  )==0.or.mask_new(j+1,i  )==1)) call add_to_q(j+1,i  )
      if (i<imax.and.j>jmin.and.(mask_new(j-1,i+1)==0.or.mask_new(j-1,i+1)==1)) call add_to_q(j-1,i+1)
      if (i<imax           .and.(mask_new(j  ,i+1)==0.or.mask_new(j  ,i+1)==1)) call add_to_q(j  ,i+1)
      if (i<imax.and.j<jmax.and.(mask_new(j+1,i+1)==0.or.mask_new(j+1,i+1)==1)) call add_to_q(j+1,i+1)
    enddo
    where(mask_new(jmin:jmax,imin:imax)>=2) mask_new(jmin:jmax,imin:imax) = 3-mask_new(jmin:jmax,imin:imax)
  end subroutine

  subroutine add_to_q(j2,i2)
    integer, intent(in) :: i2,j2
    mask_new(j2,i2) = 2+mask_new(j2,i2)
    if (mask_new(j2,i2)==2) then
      Nmask = Nmask+1
    else
      Nmask = Nmask-1
    endif
    NQ = NQ+1
    Q(NQ,1) = j2
    Q(NQ,2) = i2
  end subroutine


end subroutine mask2bnd


!=================================================================
!
!  river_segments
!
!=================================================================
subroutine river_segments(nlon,nlat,lon,lat,fdir,conv_db,Nsegments,segments)

implicit none

! Input variables
integer, intent(in)  :: nlon                     ! number of longitudes
integer, intent(in)  :: nlat                     ! number of latitudes
real*8 , intent(in)  :: lon(0:nlon-1)            ! vector of longitudes
real*8 , intent(in)  :: lat(0:nlat-1)            ! vector of latitudes
integer, intent(in)  :: fdir(0:nlat-1,0:nlon-1)  ! flow direction
character(len=4), intent(in) :: conv_db          ! flow direction convention

! Output variables
integer  , intent(out) :: Nsegments                     ! number of segments
integer  , intent(out) :: segments(1:nlon*nlat,0:1,0:1) ! coordinate of the segments

! Local variables
real*8 :: lon_ext(-1:nlon),lat_ext(-1:nlat),res
integer, dimension(1:8) :: next_i,next_j
integer :: i,j

call convention(conv_db,next_i,next_j)

res = lon(1)-lon(0)
lon_ext(0:nlon-1) = lon(:)
lon_ext(-1) = lon_ext(0)-res
lon_ext(nlon) = lon(nlon-1)+res
if (lat(1)>lat(0)) then ! increasing latitudes
  lat_ext(0:nlat-1) = lat(nlat-1:0:-1)
else                    ! decreasing latitudes
  lat_ext(0:nlat-1) = lat(:)
endif
lat_ext(-1) = lat_ext(0)+res
lat_ext(nlat) = lat_ext(nlat-1)-res

Nsegments = 0
segments(:,:,:) = 0

do i = 0,nlon-1
  do j = 0,nlat-1

    if (fdir(j,i)<1 .or. fdir(j,i)>8) cycle

    Nsegments = Nsegments+1
    segments(Nsegments,0,:) = (/i,j/)
    segments(Nsegments,1,:) = (/i+next_i(fdir(j,i)),j+next_j(fdir(j,i))/)
    !if width_min is None:
    !  rw[k] = a*(facc[j,i]-b)**c
    !else:
    !  rw[k] = 4-(4-.1)*(np.log(width_max/width[j,i])/np.log(width_max/width_min))

  enddo
enddo

end subroutine river_segments

end module basin
