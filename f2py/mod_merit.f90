module merit

! Types
type int1_type
  integer*1, allocatable, dimension(:,:) :: data
end type int1_type
type int4_type
  integer  , allocatable, dimension(:,:) :: data
end type int4_type
type real_type
  real*4   , allocatable, dimension(:,:) :: data
end type real_type


! Parameters
integer*1, parameter  , dimension(1:8) :: next_i = (/1,1,0,-1,-1,-1,0,1/)
integer*1, parameter  , dimension(1:8) :: next_j = (/0,1,1,1,0,-1,-1,-1/) ! decreasing latitude
integer  , parameter                   :: res = 1200 ! number of pixel per degree lon/lat
integer  , parameter                   :: Nmax = 10000000 ! Maximum number of pixels within the basin
integer  , parameter                   :: Ni_tile = 72, Nj_tile = 29
character(len=255), parameter          :: main_dir = '/cnrm/surface/munier/Data/Routing/MERIT/'
! character(len=255), parameter          :: main_dir = '/home/muniers/Data/Routing/MERIT/'

! Variables
real*8   , allocatable, dimension(:)   :: lon   ! vector of longitudes
real*8   , allocatable, dimension(:)   :: lat   ! vector of latitudes
integer*1, allocatable, dimension(:,:) :: mask  ! basin mask
real*8   , allocatable, dimension(:,:) :: route ! up/down-stream route (lon,lat)

integer  , allocatable, dimension(:)   :: rn_id
real*8   , allocatable, dimension(:,:) :: rn_outlet
real*4   , allocatable, dimension(:)   :: rn_area
integer  , allocatable, dimension(:)   :: rn_id_down
real*4   , allocatable, dimension(:)   :: rn_x_down
integer  , allocatable, dimension(:)   :: rn_Nroute
real*8   , allocatable, dimension(:,:) :: rn_route
real*4   , allocatable, dimension(:,:) :: rn_hypso_xS
integer  , allocatable, dimension(:)   :: rn_Ntrib
integer  , allocatable, dimension(:)   :: rn_trib_id
real*4   , allocatable, dimension(:,:) :: rn_trib_xS




contains

!=================================================================
!
!  lonlat2ij
!
!=================================================================
subroutine lonlat2ij(lon,lat,i,j)
  real*8 , intent(in)  :: lon,lat
  integer, intent(out) :: i,j
  i = nint((lon+180-1/(2.*res))*res)
  j = nint((90-1/(2.*res)-lat)*res)
end subroutine lonlat2ij


!=================================================================
!
!  ij2lonlat
!
!=================================================================
subroutine ij2lonlat(i,j,lon,lat)
  integer, intent(in)  :: i,j
  real*8 , intent(out) :: lon,lat
  lon = -180+1./real(2*res,8)+i*1./real(res,8)
  lat = 90-1./real(2*res,8)-j*1./real(res,8)
end subroutine ij2lonlat


!=================================================================
!
!  tile
!
!=================================================================
function tile(jtile,itile)
  integer, intent(in) :: itile,jtile
  character(len=7) :: tile
  character(len=1) :: ns,ew
  integer :: m,n
  m = itile*5-180
  n = 90-(jtile+1)*5
  if (n>=0) write(ns,'(A1)') 'n'
  if (n< 0) write(ns,'(A1)') 's'
  if (m>=0) write(ew,'(A1)') 'e'
  if (m< 0) write(ew,'(A1)') 'w'
  write(tile,'(A1,I2.2,A1,I3.3)') ns,abs(n),ew,abs(m)
end function


!=================================================================
!
!  basin_mask
!
!=================================================================
subroutine basin_mask(outlet)

implicit none

! Input variables
real*8, intent(in)  :: outlet(1:2) ! coordinate of the outlet (lon,lat)

! Local variables
integer :: imin,imax,jmin,jmax,nlon,nlat,i,j,k
integer :: Nmask_points,Npoints
type(int4_type), dimension(1:1000) :: mask_points
type(int1_type), dimension(0:Nj_tile-1,0:Ni_tile-1) :: forg_tile
logical :: logout=.False.
integer :: N=0

if (allocated(lon)) deallocate(lon)
if (allocated(lat)) deallocate(lat)
if (allocated(mask)) deallocate(mask)

Npoints = 0
Nmask_points = 1
allocate(mask_points(1)%data(1:Nmax,1:2))
imin = 360*res
imax = 1
jmin = 150*res
jmax = 1

call lonlat2ij(outlet(1),outlet(2),i,j)

call previous_ij(j,i)

do i = 0,Ni_tile-1
  do j = 0,Nj_tile-1
    if (allocated(forg_tile(j,i)%data)) deallocate(forg_tile(j,i)%data)
  enddo
enddo

nlon = imax-imin+1
nlat = jmax-jmin+1
allocate(lon(1:nlon))
allocate(lat(1:nlat))
allocate(mask(1:nlat,1:nlon))
do i = 1,nlon
  lon(i) = -180+1./real(2*res,8)+(imin+i-1.)/real(res,8)
enddo
do j = 1,nlat
  lat(j) = 90-1./real(2*res,8)-(jmin+j-1.)/real(res,8)
enddo
mask(:,:) = 0
do j = 1,Nmask_points
  k = Nmax
  if (j==Nmask_points) k = Npoints
  do i = 1,k
    mask(mask_points(j)%data(i,1)-jmin+1,mask_points(j)%data(i,2)-imin+1) = 1
  enddo
  deallocate(mask_points(j)%data)
enddo


contains

  recursive subroutine previous_ij(j,i)
    integer, intent(in) :: i,j
    integer*1 :: f,k
    real*8 :: x,y
    integer :: jtile,itile
    Npoints = Npoints+1
    if (Npoints>Nmax) then
      Nmask_points = Nmask_points+1
      allocate(mask_points(Nmask_points)%data(1:Nmax,1:2))
      Npoints = 1
    endif
    !if (mod(Npoints,1000)==0) write(*,*) Nmask_points,Npoints,i,j
    !if (Nmask_points == 2) stop
    !if (i==106430 .and. j==88891) logout = .True.
    !if (logout) then
    !  N = N+1
    !  itile = floor(i/(5.*res))
    !  jtile = floor(j/(5.*res))
    !  call ij2lonlat(i,j,x,y)
    !  write(*,*) Nmask_points,Npoints,tile(jtile,itile),i,j,x,y,forg(j,i)
    !endif
    !if (N>=10) stop
    !if (Npoints>3357000) then
    !  do n = 1,Npoints-1
    !    if (j==mask_points(1)%data(n,1) .and. i==mask_points(1)%data(n,2)) then
    !      itile = floor(i/(5.*res))
    !      jtile = floor(j/(5.*res))
    !      call ij2lonlat(i,j,x,y)
    !      write(*,*) Nmask_points,Npoints,tile(jtile,itile),i,j,x,y,forg(j,i)
    !      stop
    !    endif
    !  enddo
    !endif
    mask_points(Nmask_points)%data(Npoints,:) = (/j,i/)
    if (i<imin) imin = i
    if (i>imax) imax = i
    if (j<jmin) jmin = j
    if (j>jmax) jmax = j
    f = forg(j,i)
    do k = 1,8
      if (btest(f,k-1)) call previous_ij(j+next_j(k),i+next_i(k))
    enddo
  end subroutine previous_ij

  function forg(j,i)
    integer, intent(in) :: i,j
    integer*1 :: forg
    integer :: jtile,itile
    itile = floor(i/(5.*res))
    jtile = floor(j/(5.*res))
    if (.not. allocated(forg_tile(jtile,itile)%data) .and. .not. read_tile(jtile,itile)) then
      forg = 0
    else
      forg = forg_tile(jtile,itile)%data(i-itile*5*res,j-jtile*5*res)
    endif
  end function forg

  function read_tile(jtile,itile)
    integer, intent(in) :: jtile,itile
    logical :: read_tile
    character(len=255) :: fname
    write(fname,'(A255)') trim(main_dir)//'org_3s/'//tile(jtile,itile)//'_org.bin'
    fname = adjustl(fname)
    inquire(file=trim(fname),exist=read_tile)
    if (.not. read_tile) then
      write(*,*) 'Following file does not exist: '//trim(fname)
      return
    endif
    open(100,file=trim(fname),access='direct',recl=6000*6000,status='old')
    allocate(forg_tile(jtile,itile)%data(0:5999,0:5999))
    read(100,rec=1) forg_tile(jtile,itile)%data(:,:)
    close(100)
  endfunction

end subroutine basin_mask


!=================================================================
!
!  main_upstream
!
!=================================================================
subroutine main_upstream(outlet)

implicit none

! Input variables
real*8, intent(in)  :: outlet(1:2) ! coordinate of the outlet (lon,lat)

! Local variables
integer :: i,j,k
integer :: Nroute_points,Npoints
type(int1_type), dimension(0:Nj_tile-1,0:Ni_tile-1) :: forg_tile
type(real_type), dimension(0:Nj_tile-1,0:Ni_tile-1) :: facc_tile
type(int4_type), dimension(1:1000) :: route_points


if (allocated(route)) deallocate(route)

Npoints = 0
Nroute_points = 1
allocate(route_points(1)%data(1:Nmax,1:2))

call lonlat2ij(outlet(1),outlet(2),i,j)

call previous_ij(j,i)

do i = 0,Ni_tile-1
  do j = 0,Nj_tile-1
    if (allocated(forg_tile(j,i)%data)) deallocate(forg_tile(j,i)%data)
    if (allocated(facc_tile(j,i)%data)) deallocate(facc_tile(j,i)%data)
  enddo
enddo

allocate(route(1:Npoints+(Nroute_points-1)*Nmax,1:2))
do j = 1,Nroute_points
  k = Nmax
  if (j==Nroute_points) k = Npoints
  do i = 1,k
    call ij2lonlat(route_points(j)%data(i,1),route_points(j)%data(i,2),&
                   route(i+(j-1)*Nmax,1),route(i+(j-1)*Nmax,2))
  enddo
  deallocate(route_points(j)%data)
enddo


contains

  recursive subroutine previous_ij(j,i)
    integer, intent(in) :: i,j
    integer*1 :: f,k,kup
    real*4 :: Sup
    Npoints = Npoints+1
    if (Npoints>Nmax) then
      Nroute_points = Nroute_points+1
      allocate(route_points(Nroute_points)%data(1:Nmax,1:2))
      Npoints = 1
    endif
    route_points(Nroute_points)%data(Npoints,:) = (/i,j/)
    f = forg(j,i)
    Sup = 0
    kup = 0
    do k = 1,8
      if (btest(f,k-1) .and. facc(j+next_j(k),i+next_i(k))>Sup) then
        kup = k
        Sup = facc(j+next_j(k),i+next_i(k))
      endif
    enddo
    if (Sup>0) call previous_ij(j+next_j(kup),i+next_i(kup))
  end subroutine previous_ij

  function forg(j,i)
    integer, intent(in) :: i,j
    integer*1 :: forg
    integer :: jtile,itile
    itile = floor(i/(5.*res))
    jtile = floor(j/(5.*res))
    if (.not. allocated(forg_tile(jtile,itile)%data) .and. .not. read_tile_forg(jtile,itile)) then
      forg = 0
    else
      forg = forg_tile(jtile,itile)%data(i-itile*5*res,j-jtile*5*res)
    endif
  end function forg

  function facc(j,i)
    integer, intent(in) :: i,j
    real*4 :: facc
    integer :: jtile,itile
    itile = floor(i/(5.*res))
    jtile = floor(j/(5.*res))
    if (.not. allocated(facc_tile(jtile,itile)%data) .and. .not. read_tile_facc(jtile,itile)) then
      facc = 0.
    else
      facc = facc_tile(jtile,itile)%data(i-itile*5*res,j-jtile*5*res)
    endif
  end function facc

  function read_tile_forg(jtile,itile)
    integer, intent(in) :: jtile,itile
    logical :: read_tile_forg
    character(len=255) :: fname
    write(fname,'(A255)') trim(main_dir)//'org_3s/'//tile(jtile,itile)//'_org.bin'
    fname = adjustl(fname)
    inquire(file=trim(fname),exist=read_tile_forg)
    if (.not. read_tile_forg) then
      write(*,*) 'Following file does not exist: '//trim(fname)
      return
    endif
    open(100,file=trim(fname),access='direct',recl=6000*6000,status='old')
    allocate(forg_tile(jtile,itile)%data(0:5999,0:5999))
    read(100,rec=1) forg_tile(jtile,itile)%data(:,:)
    close(100)
  end function read_tile_forg

  function read_tile_facc(jtile,itile)
    integer, intent(in) :: jtile,itile
    logical :: read_tile_facc
    character(len=255) :: fname
    write(fname,'(A255)') trim(main_dir)//'upa_3s/'//tile(jtile,itile)//'_upa.bin'
    fname = adjustl(fname)
    inquire(file=trim(fname),exist=read_tile_facc)
    if (.not. read_tile_facc) then
      write(*,*) 'Following file does not exist: '//trim(fname)
      return
    endif
    open(100,file=trim(fname),access='direct',recl=4*6000*6000,status='old')
    allocate(facc_tile(jtile,itile)%data(0:5999,0:5999))
    read(100,rec=1) facc_tile(jtile,itile)%data(:,:)
    close(100)
  end function read_tile_facc

end subroutine main_upstream


!=================================================================
!
!  main_downstream
!
!=================================================================
subroutine main_downstream(inlet)

implicit none

! Input variables
real*8, intent(in) :: inlet(1:2) ! coordinate of the inlet (lon,lat)

! Local variables
integer :: i,j,k
integer :: Nroute_points,Npoints
type(int1_type), dimension(0:Nj_tile-1,0:Ni_tile-1) :: fdir_tile
type(int4_type), dimension(1:1000) :: route_points

if (allocated(route)) deallocate(route)

Npoints = 0
Nroute_points = 1
allocate(route_points(1)%data(1:Nmax,1:2))

call lonlat2ij(inlet(1),inlet(2),i,j)

call next_ij(j,i)

do i = 0,Ni_tile-1
  do j = 0,Nj_tile-1
    if (allocated(fdir_tile(j,i)%data)) deallocate(fdir_tile(j,i)%data)
  enddo
enddo

allocate(route(1:Npoints+(Nroute_points-1)*Nmax,1:2))
do j = 1,Nroute_points
  k = Nmax
  if (j==Nroute_points) k = Npoints
  do i = 1,k
    call ij2lonlat(route_points(j)%data(i,1),route_points(j)%data(i,2),&
                   route(i+(j-1)*Nmax,1),route(i+(j-1)*Nmax,2))
  enddo
  deallocate(route_points(j)%data)
enddo


contains

  recursive subroutine next_ij(j,i)
    integer, intent(in) :: i,j
    integer*1 :: f
    Npoints = Npoints+1
    if (Npoints>Nmax) then
      Nroute_points = Nroute_points+1
      allocate(route_points(Nroute_points)%data(1:Nmax,1:2))
      Npoints = 1
    endif
    route_points(Nroute_points)%data(Npoints,:) = (/i,j/)
    f = fdir(j,i)
    if (f>0 .and. f<9) call next_ij(j+next_j(f),i+next_i(f))
  end subroutine next_ij

  function fdir(j,i)
    integer, intent(in) :: i,j
    integer*1 :: fdir,tmp
    integer :: jtile,itile
    itile = floor(i/(5.*res))
    jtile = floor(j/(5.*res))
    if (.not. allocated(fdir_tile(jtile,itile)%data) .and. .not. read_tile(jtile,itile)) then
      tmp = 0
    else
      tmp = fdir_tile(jtile,itile)%data(i-itile*5*res,j-jtile*5*res)
    endif
    if (tmp==0 .or. tmp==-9 .or. tmp==-1) then
      fdir = 0
    else
      fdir = 1
      do while (tmp/=1)
        fdir = fdir+1
        tmp = shiftr(tmp,1)
      enddo
    endif
  end function fdir

  function read_tile(jtile,itile)
    integer, intent(in) :: jtile,itile
    logical :: read_tile
    character(len=255) :: fname
    write(fname,'(A255)') trim(main_dir)//'dir_3s/'//tile(jtile,itile)//'_dir.bin'
    fname = adjustl(fname)
    inquire(file=trim(fname),exist=read_tile)
    if (.not. read_tile) then
      write(*,*) 'Following file does not exist: '//trim(fname)
      return
    endif
    open(100,file=trim(fname),access='direct',recl=6000*6000,status='old')
    allocate(fdir_tile(jtile,itile)%data(0:5999,0:5999))
    read(100,rec=1) fdir_tile(jtile,itile)%data(:,:)
    close(100)
  endfunction

end subroutine main_downstream


!=================================================================
!
!  river_network
!
!=================================================================
subroutine river_network(main_outlet,inlet,Smin)

implicit none

! Parameters
integer, parameter :: Nrivmax = 10000
integer, parameter :: Nroutemax = Nmax

! Input variables
real*8, intent(in), dimension(1:2) :: main_outlet ! coordinate of the outlet (lon,lat)
real*8, intent(in), dimension(:,:) :: inlet       ! coordinate of the inlets (lon,lat)
real*4, intent(in)                 :: Smin        ! minimum area

! Output variables
integer :: Nriv
integer, dimension(1:Nrivmax)           :: id
real*8 , dimension(1:Nrivmax,1:2)       :: outlet
real*4 , dimension(1:Nrivmax)           :: area
integer, dimension(1:Nrivmax)           :: id_down
real*4 , dimension(1:Nrivmax)           :: x_down
integer, dimension(1:Nrivmax)           :: Nroute
real*8 , dimension(1:Nroutemax,1:2)     :: route_xy
real*4 , dimension(1:Nroutemax,1:2)     :: hypso
integer, dimension(1:Nrivmax)           :: Ntrib
integer, dimension(1:Nrivmax,1:Nrivmax) :: trib_id
real*4 , dimension(1:Nrivmax,1:Nrivmax) :: trib_x
real*4 , dimension(1:Nrivmax,1:Nrivmax) :: trib_S

! Local variables
integer :: i,j,k
type(int1_type), dimension(0:Nj_tile-1,0:Ni_tile-1) :: forg_tile
type(real_type), dimension(0:Nj_tile-1,0:Ni_tile-1) :: facc_tile
integer :: Nin,Nroute_all,Ntrib_all
integer, allocatable :: inlet_ij(:,:)
real*4 :: minimum_area

if (allocated(rn_id)) deallocate(rn_id)
if (allocated(rn_outlet)) deallocate(rn_outlet)
if (allocated(rn_area)) deallocate(rn_area)
if (allocated(rn_id_down)) deallocate(rn_id_down)
if (allocated(rn_x_down)) deallocate(rn_x_down)
if (allocated(rn_Nroute)) deallocate(rn_Nroute)
if (allocated(rn_route)) deallocate(rn_route)
if (allocated(rn_hypso_xS)) deallocate(rn_hypso_xS)
if (allocated(rn_Ntrib)) deallocate(rn_Ntrib)
if (allocated(rn_trib_id)) deallocate(rn_trib_id)
if (allocated(rn_trib_xS)) deallocate(rn_trib_xS)


Nriv = 1
Nroute_all = 0
Nroute(:) = 0
Ntrib_all = 0
Ntrib(:) = 0


! Get inlet indices
Nin = size(inlet,1)
allocate(inlet_ij(1:Nin,1:2))
do i = 1,Nin
  call lonlat2ij(inlet(i,1),inlet(i,2),inlet_ij(i,1),inlet_ij(i,2))
enddo

! First sub-basin
call lonlat2ij(main_outlet(1),main_outlet(2),i,j)
id(1) = 1
call ij2lonlat(i,j,outlet(1,1),outlet(1,2))
area(1) = facc(j,i)
id_down(1) = -1
x_down(1) = -1

! Get minimum area
minimum_area = Smin
if (minimum_area<0) minimum_area = 0.01*facc(j,i)

! Recursive call
call stream_network(1,i,j)

deallocate(inlet_ij)
do i = 0,Ni_tile-1
  do j = 0,Nj_tile-1
    if (allocated(forg_tile(j,i)%data)) deallocate(forg_tile(j,i)%data)
    if (allocated(facc_tile(j,i)%data)) deallocate(facc_tile(j,i)%data)
  enddo
enddo

allocate(rn_id(1:Nriv))
allocate(rn_outlet(1:Nriv,1:2))
allocate(rn_area(1:Nriv))
allocate(rn_id_down(1:Nriv))
allocate(rn_x_down(1:Nriv))
allocate(rn_Nroute(1:Nriv))
allocate(rn_route(1:Nroute_all,1:2))
allocate(rn_hypso_xS(1:Nroute_all,1:2))
allocate(rn_Ntrib(1:Nriv))
allocate(rn_trib_id(1:Ntrib_all))
allocate(rn_trib_xS(1:Ntrib_all,1:2))

rn_id(:) = id(1:Nriv)
rn_outlet(:,:) = outlet(1:Nriv,:)
rn_area(:) = area(1:Nriv)
rn_id_down(:) = id_down(1:Nriv)
rn_x_down(:) = x_down(1:Nriv)
rn_Nroute(:) = Nroute(1:Nriv)
rn_route(:,:) = route_xy(1:Nroute_all,:)
rn_hypso_xS(:,:) = hypso(1:Nroute_all,:)
rn_Ntrib(:) = Ntrib(1:Nriv)
Ntrib_all = 0
do k = 1,Nriv
  rn_trib_id(Ntrib_all+1:Ntrib_all+Ntrib(k)) = trib_id(k,1:Ntrib(k))
  rn_trib_xS(Ntrib_all+1:Ntrib_all+Ntrib(k),1) = trib_x(k,1:Ntrib(k))
  rn_trib_xS(Ntrib_all+1:Ntrib_all+Ntrib(k),2) = trib_S(k,1:Ntrib(k))
  Ntrib_all = Ntrib_all+Ntrib(k)
enddo


contains


  recursive subroutine stream_network(iRN,i,j)
    implicit none
    integer, intent(in) :: iRN,i,j
    integer :: f,k,distance,kup,in,jn
    real*4 :: Sup
    ! Add point to route
    Nroute_all = Nroute_all+1
    if (Nroute_all>Nroutemax) then
      write(*,*) 'Maximum number of route points reached:',Nroute_all
      return
    endif
    Nroute(iRN) = Nroute(iRN)+1
    call ij2lonlat(i,j,route_xy(Nroute_all,1),route_xy(Nroute_all,2))
    ! Add hypsometry (distance is in number of pixels -> could be improved)
    if (Nroute(iRN)==1) then
      distance = 0
    else
      distance = hypso(Nroute_all-1,1)+1
    endif
    hypso(Nroute_all,:) = (/real(distance),facc(j,i)/)
    do k = 1,Nin
      if (i==inlet_ij(k,1) .and. j==inlet_ij(k,2)) return
    enddo
    ! Build main upstream network
    f = forg(j,i)
    Sup = 0
    do k = 1,8
      if (btest(f,k-1) .and. facc(j+next_j(k),i+next_i(k))>=Sup) then
        kup = k
        Sup = facc(j+next_j(k),i+next_i(k))
      endif
    enddo
    if (Sup<=minimum_area) return
    call stream_network(iRN,i+next_i(kup),j+next_j(kup))
    ! Build tributaries
    do k = 1,8
      if (k/=kup .and. btest(f,k-1) .and. facc(j+next_j(k),i+next_i(k))>=minimum_area) then
        Nriv = Nriv+1
        if (Nriv>Nrivmax) then
          write(*,*) 'Maximum number of rivers reached:',Nriv
          return
        endif
        in = i+next_i(k)
        jn = j+next_j(k)
        id(Nriv) = Nriv
        call ij2lonlat(in,jn,outlet(Nriv,1),outlet(Nriv,2))
        area(Nriv) = facc(jn,in)
        id_down(Nriv) = iRN
        x_down(Nriv) = distance
        Nroute_all = Nroute_all+1
        if (Nroute_all>Nroutemax) then
          write(*,*) 'Maximum number of route points reached:',Nroute_all
          return
        endif
        Nroute(Nriv) = Nroute(Nriv)+1
        call ij2lonlat(in,jn,route_xy(Nroute_all,1),route_xy(Nroute_all,2))
        Ntrib_all = Ntrib_all+1
        if (Ntrib_all>Nrivmax) then
          write(*,*) 'Maximum number of tributaries reached:',Ntrib_all
          return
        endif
        Ntrib(iRN) = Ntrib(iRN)+1
        trib_id(iRN,Ntrib(iRN)) = Nriv
        trib_x(iRN,Ntrib(iRN)) = distance
        trib_S(iRN,Ntrib(iRN)) = facc(jn,in)
        call stream_network(Nriv,in,jn)
      endif
    enddo
  end subroutine stream_network

  function forg(j,i)
    integer, intent(in) :: i,j
    integer*1 :: forg
    integer :: jtile,itile
    itile = floor(i/(5.*res))
    jtile = floor(j/(5.*res))
    if (.not. allocated(forg_tile(jtile,itile)%data) .and. .not. read_tile_forg(jtile,itile)) then
      forg = 0
    else
      forg = forg_tile(jtile,itile)%data(i-itile*5*res,j-jtile*5*res)
    endif
  end function forg

  function facc(j,i)
    integer, intent(in) :: i,j
    real*4 :: facc
    integer :: jtile,itile
    itile = floor(i/(5.*res))
    jtile = floor(j/(5.*res))
    if (.not. allocated(facc_tile(jtile,itile)%data) .and. .not. read_tile_facc(jtile,itile)) then
      facc = 0.
    else
      facc = facc_tile(jtile,itile)%data(i-itile*5*res,j-jtile*5*res)
    endif
  end function facc

  function read_tile_forg(jtile,itile)
    integer, intent(in) :: jtile,itile
    logical :: read_tile_forg
    character(len=255) :: fname
    write(fname,'(A255)') trim(main_dir)//'org_3s/'//tile(jtile,itile)//'_org.bin'
    fname = adjustl(fname)
    inquire(file=trim(fname),exist=read_tile_forg)
    if (.not. read_tile_forg) then
      write(*,*) 'Following file does not exist: '//trim(fname)
      return
    endif
    open(100,file=trim(fname),access='direct',recl=6000*6000,status='old')
    allocate(forg_tile(jtile,itile)%data(0:5999,0:5999))
    read(100,rec=1) forg_tile(jtile,itile)%data(:,:)
    close(100)
  end function read_tile_forg

  function read_tile_facc(jtile,itile)
    integer, intent(in) :: jtile,itile
    logical :: read_tile_facc
    character(len=255) :: fname
    write(fname,'(A255)') trim(main_dir)//'upa_3s/'//tile(jtile,itile)//'_upa.bin'
    fname = adjustl(fname)
    inquire(file=trim(fname),exist=read_tile_facc)
    if (.not. read_tile_facc) then
      write(*,*) 'Following file does not exist: '//trim(fname)
      return
    endif
    open(100,file=trim(fname),access='direct',recl=4*6000*6000,status='old')
    allocate(facc_tile(jtile,itile)%data(0:5999,0:5999))
    read(100,rec=1) facc_tile(jtile,itile)%data(:,:)
    close(100)
  end function read_tile_facc


end subroutine river_network


!=================================================================
!
!  cleanup
!
!=================================================================
subroutine cleanup()

  if (allocated(lon)) deallocate(lon)
  if (allocated(lat)) deallocate(lat)
  if (allocated(mask)) deallocate(mask)
  if (allocated(route)) deallocate(route)
  if (allocated(rn_id)) deallocate(rn_id)
  if (allocated(rn_outlet)) deallocate(rn_outlet)
  if (allocated(rn_area)) deallocate(rn_area)
  if (allocated(rn_id_down)) deallocate(rn_id_down)
  if (allocated(rn_x_down)) deallocate(rn_x_down)
  if (allocated(rn_Nroute)) deallocate(rn_Nroute)
  if (allocated(rn_route)) deallocate(rn_route)
  if (allocated(rn_hypso_xS)) deallocate(rn_hypso_xS)
  if (allocated(rn_Ntrib)) deallocate(rn_Ntrib)
  if (allocated(rn_trib_id)) deallocate(rn_trib_id)
  if (allocated(rn_trib_xS)) deallocate(rn_trib_xS)

end subroutine cleanup



end module merit

!program main
!
!use merit
!
!real*8 :: outlet(1:2)=(/-91.487916666667,17.43291666667/)
!
!
!call basin_mask(outlet)
!!call basin_mask((/-123.23125,41.85375/))
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! call basin((/-50.73875046,-0.43541667/))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!end program main
