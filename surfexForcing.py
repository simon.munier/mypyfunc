# ----------------------------------------------
# IMPORT LIBRARIES
# ----------------------------------------------
import pandas as pd
import numpy as np
import mpi4py.MPI as mpi
import os
from netCDF4 import Dataset
from sys import argv
from glob import glob
from .tqdm import tqdm, trange
from .misc import res2lonlat, get_IlonIlat, var_resample
from .mod_oasis import oasis as foasis

# ----------------------------------------------
# SET CONSTANTS
# ----------------------------------------------
# All SFX variables needed
SFX_VARS_UNITS = {'Tair':'K', 'Qair':'kg/kg', 'Wind':'m/s', 'DIR_SWdown':'W/m2', 'LWdown':'W/m2', 'Rainf':'kg/m2/s',\
                   'Snowf':'kg/m2/s' , 'PSurf':'Pa' ,'CO2air':'kg/m3', 'SCA_SWdown':'W/m2' ,'Wind_DIR':'deg'}
FORC_VAR_ERA5  = {'Tair':'Tair', 'Qair':'Qair', 'Wind':'Wind', 'DIR_SWdown':'SWdown', 'LWdown':'LWdown',
                  'Rainf':'Rainf', 'Snowf':'Snowf', 'PSurf':'PSurf'}
freq_forcing = 1 # hourly forcings for ERA5
# Location of ancillary data
data_dir = os.path.realpath(os.path.split(__file__)[0])+'/data/'
era5_dir_default = '/cnrm/vegeo/LDAS/CEP/forcing_ea_nc'

# ----------------------------------
# PUBLIC FUNCTIONS
# ----------------------------------

def createSIM4CTRIP(isba_dir,trip_dir):
    '''
    '''
    zone = [[-5,51],[10,42]]
    res = 1./12
    Nsim = 9892
    lon,lat,nlon,nlat = res2lonlat(res,zone,latinvert=True)

    # OASIS remap construction
    with Dataset(data_dir+'remap_SAFRAN_to_CTRIP.nc') as nc:
      src_add = nc['src_address'][:]
      dst_add = nc['dst_address'][:]
      rmp = nc['remap_matrix'][:,0]

    # ISBA outputs
    isba_files = sorted(glob(isba_dir+'/DRAINC*BIN*'))
    for f in isba_files:
      year = int(f[-9:-5])
      trip_file = trip_dir+'TRIP_FORCING_{:d}.nc'.format(year)
      print(trip_file)
      drainc = np.fromfile(f,'>f4').reshape((-1,Nsim))
      runoffc = np.fromfile(f.replace('DRAIN','RUNOFF'),'>f4').reshape((-1,Nsim))
      ndat = drainc.shape[0]
      time_units = 'hours since {}-08-01 06:00:00'.format(year)
      dat = np.arange(ndat)
      drainc_trip = foasis.oasis_remap_field(src_add,dst_add,rmp,drainc,nlon*nlat)
      runoffc_trip = foasis.oasis_remap_field(src_add,dst_add,rmp,runoffc,nlon*nlat)
      drainc_trip = drainc_trip.reshape((ndat,nlat,nlon))
      runoffc_trip = runoffc_trip.reshape((ndat,nlat,nlon))
      with Dataset(trip_file,'w') as nc:
        nc.createDimension('longitude',nlon)
        nc.createDimension('latitude',nlat)
        nc.createDimension('time',None)
        nc.createVariable('longitude','f4','longitude')
        nc.createVariable('latitude','f4','latitude')
        nc.createVariable('time','f4','time')
        nc['longitude'][:] = lon
        nc['latitude'][:] = lat
        nc['time'].units = time_units
        nc['time'][:] = dat
        nc.createVariable('DRAIN','f4',('time','latitude','longitude'))
        nc['DRAIN'][:] = drainc_trip
        nc.createVariable('RUNOFF','f4',('time','latitude','longitude'))
        nc['RUNOFF'][:] = runoffc_trip


def createMonthlyForcing(zone,period,forcing_dir,era5_dir=era5_dir_default,nc_version='NETCDF3_64BIT',ravel=True,res='QD'):
    '''
    Retrieve raw forcing files from era5_dir, select the given area and add the missing fields.
    Monthly forcing.
    Arguments:
        zone (list): domain limits [[lon_min,lat_max],[lon_max,lat_min]]
        period (list): start and end dates for pre-processing (str: yyyy-mm-dd).
        forcing_dir (str): directory where forcing files are written
        era5_dir (str): directory from where the raw forcing data are read
        nc_version (str): NetCDF version for output ('NETCDF4', 'NETCDF4_CLASSIC', 'NETCDF3_64BIT', 'NETCDF3_CLASSIC'), read from option file
        ravel (bool): if True, space dimension in Number_of_points, else (lat,lon)
    '''
    #
    # 0-Read ZS from global PGD nc file
    if res=='QD':
        zs_file = data_dir+'ZS_QD_gtopo30.nc'
    elif res=='HD':
        zs_file = data_dir+'ZS_HD_gtopo30.nc'
    with Dataset(zs_file) as nc:
        lon,lat = nc['REG_LON'][0,:],nc['REG_LAT'][:,0]
        Ilon,Ilat = get_IlonIlat(lon,lat,zone)
        lon,lat = lon[Ilon],lat[Ilat]
        lon,lat = np.meshgrid(lon,lat)
        mod_ZS = nc['ZS'][Ilat,Ilon]
        if ravel: mod_ZS = mod_ZS.ravel()
    #
    # 1-Find the temporal range of the raw forcing data to read
    months = pd.date_range(*period,freq='M').to_period()
    #
    # 2-LAT/LON slice => nb_points if ravel
    nc_filename = os.path.normpath('{0}/Tair/{1}'.format(era5_dir,os.listdir(era5_dir+'/Tair/')[0]))
    # Get longitude and latitude vectors (must be named as lon* and lat* in forcing files)
    with Dataset(nc_filename, 'r') as nc:
        nam_lon = [v for v in nc.variables.keys() if 'lon' in v[:3].lower()][0]
        nam_lat = [v for v in nc.variables.keys() if 'lat' in v[:3].lower()][0]
        src_lon, src_lat = nc.variables[nam_lon][:], nc.variables[nam_lat][:]
    src_lon = np.mod(src_lon+180.,360.)-180.
    # Select zone
    print('')
    print('Spatial domain: latmin = ',zone[1][1],'latmax = ',zone[0][1],'lonmin = ',zone[0][0],'lonmax = ',zone[1][0])
    print('')
    Ilon,Ilat = get_IlonIlat(src_lon,src_lat,zone)
    j_inc = 1 if src_lat[1]>src_lat[0] else -1
    #
    # 3-Read CCO2 annual estimations and constants
    co2_year_ppmv_file = data_dir+'GHG_1765_2500_A2.txt'
    co2_year_ppmv = np.genfromtxt(co2_year_ppmv_file)[:,0:1+1] # years 1765-2500
    M_CO2     = 44.010e-3    # molar mass [kg/mol] of CO2
    M_air     = 28.965e-3    # molar mass [kg/mol] of air
    M_wat     = 18.015e-3    # molar mass [kg/mol] of water
    gravity   = 9.807        # gravity [m/s2]
    NA        = 6.022e23     # Avogrado number [-] (number of entities in a mole)
    kB        = 1.381e-23    # Boltzman constant [J/K] (physical constant relating energy at the individual particle level with temperature)
    r_dry_air = NA*kB/M_air  # specific gas constant [J/kg/K] for dry air
    r_wat_vap = NA*kB/M_wat  # specific gas constant [J/kg/K] for water vapor contained in the air (humidity)
    height_for_Tair = 2.0    # Tair is at 2 meters
    #
    # 4-Extract from monthly raw forcing and add missing fields
    print('*******************************')
    print('Extracting ROI from raw forcing')
    print('AND adding missing fields from')
    print('{0} to {1}'.format(period[0],period[1]))
    print('*******************************')
    print('')
    #
    if mpi.COMM_WORLD.rank==0:
        if not os.path.isdir('{0}'.format(forcing_dir)): os.makedirs('{0}'.format(forcing_dir))
    mpi.COMM_WORLD.barrier()
    icpu = mpi.COMM_WORLD.rank
    ncpu = mpi.COMM_WORLD.size
    nmon = len(months)//ncpu
    if icpu < len(months)%ncpu:
        nmon += 1
        imon = nmon*icpu
    else:
        imon = nmon*icpu + len(months)%ncpu
    #
    MISS_VARS = set(SFX_VARS_UNITS.keys())-set(FORC_VAR_ERA5.keys())
    #if 'SCA_SWdown' in MISS_VARS:
    #    print('SCA_SWdown variable is missing in forcing file, assigning it to 0 W/m2')
    #if 'Wind_DIR' in MISS_VARS:
    #    print('Wind_DIR variable is missing in forcing file, assigning it to 180 degrees')
    #if 'CO2air' in MISS_VARS:
    #    print('CO2air variable is missing in forcing file, calculating it using Psurf, Tair, Qair, ppmv/year')
    #
    for curr_month in tqdm(months[imon:imon+nmon]):
        #
        # A - Time info
        start = curr_month.start_time
        end   = curr_month.end_time + pd.DateOffset(minutes=1)
        time  = pd.date_range(start, end, freq='{0}H'.format(freq_forcing))
        #
        # B - Init raw netcdf file to write
        monthly_file = '{0}/FORCING_{1}.nc'.format(forcing_dir,curr_month.strftime('%Y%m'))
        if os.path.isfile(monthly_file): continue
        monthly_data = _initializeNetCDF(monthly_file,nc_version,time,lon,lat,mod_ZS,ravel)
        #
        IC = 0
        NT = len(time)
        NC = int(24/freq_forcing)
        for IA in range(0, NT, NC):
            IB = min(IA + NC,NT)
            #
            # C - Extract forcing data
            for sfx_var,forc_var in FORC_VAR_ERA5.items():
                raw_files = glob('{0}/{1}/*{2}*.nc'.format(era5_dir,forc_var,curr_month.strftime('%Y%m')))
                raw_files = sorted(raw_files)
                if len(raw_files)==0:
                    raise Exception('No monthly forcing file for date {0}, variable {1} in {2}'.format(curr_month.strftime('%Y%m'),forc_var,era5_dir))
                raw_file = os.path.normpath(raw_files[0])
                raw_data = Dataset(raw_file, 'r')
                # Dimensions should be checked first instead of being hard-coded ...
                if raw_data.variables[forc_var].ndim == 3:
                    tmp = raw_data.variables[forc_var][IA:IB,Ilat,Ilon]
                else:
                    tmp = raw_data.variables[forc_var][IA:IB,0,Ilat,Ilon]
                if j_inc < 0: tmp = tmp[:,::j_inc,:]
                if ravel: tmp = tmp.reshape((IB-IA, -1))
                if res=='HD': tmp = var_resample(tmp,2,rdim=[1,2],pmin=0)
                monthly_data.variables[sfx_var][IA:IB] = tmp
                # in E2O forcing file (probably all ECMWF forcing), Tair and Qair have one more dimension : (time, height, lat, lon)
                if sfx_var=='Tair': Tair = tmp
                elif sfx_var=='Qair': Qair = tmp
                if sfx_var=='PSurf': PSurf = tmp
                raw_data.close()
            #
            # D - Find the missing variables and assign them when possible
            if 'SCA_SWdown' in MISS_VARS:
                monthly_data.variables['SCA_SWdown'][IA:IB] = np.zeros((IB-IA,)+mod_ZS.shape)
            if 'Wind_DIR' in MISS_VARS:
                monthly_data.variables['Wind_DIR'] [IA:IB] = 180.*np.ones((IB-IA,)+mod_ZS.shape)
            #
            # E - CALCULATION OF CO2 CONCENTRATION [ppmv to kg/m3] if needed
            if 'CO2air' in MISS_VARS:
                '''
                ## Psurf = Psoil                               +  rho_air_moist * g * height
                ##       = rho_air_moist * r_air_moist * Tair  +  rho_air_moist * g * height
                ##       = rho_air_moist * (r_air_moist * Tair + g * height)
                ## => rho_air_moist = Psurf / (r_air_moist * Tair + g * height)
                ##    with: r_air_moist = r_dry_air * (1+ ((r_wat_vap/r_dry_air) -1)*Qs ) with Qs:specific humidity
                ## C[CO2 in kg/m3] = ppmv*1e-6 * (M_CO2/M_air) * rho_air_moist 
                ##   (1st multiplication with molar masses to transform volume ratio into mass ratio)
                ##   (2nd multiplication with density to transforme air (mix with water) mass into volume 
                ##    to get C[CO2] in mass of CO2 per volume of air)
                '''
                r_air_moist     = r_dry_air * (1+ ((r_wat_vap/r_dry_air) -1)*Qair)
                rho_air_moist   = PSurf / (r_air_moist*Tair + gravity * height_for_Tair)
                CCO2            = co2_year_ppmv[curr_month.year-1765,1]*1e-6 * (M_CO2/M_air) * rho_air_moist
                monthly_data.variables['CO2air'][IA:IB] = CCO2
            #
        # check
        OTHER_MISS_VARS = set(MISS_VARS)-set(['SCA_SWdown','Wind_DIR','CO2air'])
        if len(OTHER_MISS_VARS)>0:
            raise Exception('Forcing file cannot be produced because of missing variables : {0}'.format(OTHER_MISS_VARS))
        #
        # close
        monthly_data.close()




# ----------------------------------------------
# PRIVATE FUNCTIONS (CALLED BY PUBLIC FUNCTIONS)
# ----------------------------------------------

def _initializeNetCDF(nc_filename,nc_version,time,inLON,inLAT,inZS,ravel):
    '''
    Initialize forcing netcdf file for SFX with surface variables
    Arguments:
        nc_filename (str): name of the netcdf file to create
        nc_version (str): NetCDF version of forcing files ['NETCDF4', 'NETCDF4_CLASSIC', 'NETCDF3_64BIT', 'NETCDF3_CLASSIC']
        time (pandas.tseries.index.DatetimeIndex): pd.date_range of the time steps
        inLON,inLAT,inZS (): 
        ravel
    Returns:
        nc_file ():
    '''
    #
    nc_file = Dataset(nc_filename,'w',format=nc_version)
    #
    nc_file.createDimension('time', len(time))
    space_shape = inZS.shape
    if ravel:
        space_dim = ('Number_of_points',)
        nc_file.createDimension('Number_of_points', space_shape[0])
    else:
        space_dim = ('lat','lon')
        nc_file.createDimension('lat', space_shape[0])
        nc_file.createDimension('lon', space_shape[1])
    #
    nc_file.createVariable('time', float, ('time', ))
    nc_file.variables['time'].setncattr('units', 'hours since ' + time[0].strftime('%Y-%m-%d %H:%M:%S')) 
    nc_file.variables['time'][:] = np.array([(time[i]-time[0]).total_seconds()/3600. for i in np.arange(len(time))]).astype(float)
    #
    nc_file.createVariable('FRC_TIME_STP',float)
    nc_file.variables['FRC_TIME_STP'].setncattr('units', 's')
    nc_file.variables['FRC_TIME_STP'].assignValue(float((time[1]-time[0]).total_seconds()))
    #
    nc_file.createVariable('LON',float,space_dim)
    nc_file.variables['LON'].setncattr('units','degrees')
    nc_file.variables['LON'][:] = inLON
    #
    nc_file.createVariable('LAT',float,space_dim)
    nc_file.variables['LAT'].setncattr('units','degrees')
    nc_file.variables['LAT'][:] = inLAT
    #
    nc_file.createVariable('ZS',float,space_dim)
    nc_file.variables['ZS'].setncattr('units','m')
    nc_file.variables['ZS'][:] = inZS
    #
    nc_file.createVariable('UREF',float,space_dim)
    nc_file.variables['UREF'].setncattr('units','m')
    nc_file.variables['UREF'][:] = 10.*np.ones(space_shape)
    #
    nc_file.createVariable('ZREF',float,space_dim)
    nc_file.variables['ZREF'].setncattr('units','m')
    nc_file.variables['ZREF'][:] = 2.*np.ones(space_shape)
    #
    for sfx_key_var in SFX_VARS_UNITS.keys():
        nc_file.createVariable(sfx_key_var,float,('time',)+space_dim,zlib=True,complevel=1,shuffle=False)
        nc_file.variables[sfx_key_var].setncattr('units',SFX_VARS_UNITS[sfx_key_var])
    return nc_file



# ----------------------------------------------
# MAIN PROGRAM
# ----------------------------------------------

if __name__ == '__main__':
    # Check if the options file is passed correctly, read it, and check for missing options
    try:
        options_file = argv[1]
        exec(open(options_file).read())
    except:
        raise Exception('\nError in the options file!')
    if 'zone' not in locals() or 'period' not in locals() or 'forcing_dir' not in locals():
        raise Exception('\nThe options file given as argument must contain "zone", "period" and "forcing_dir".')
    # Set up default options
    if 'era5_dir' not in locals(): era5_dir = era5_dir_default
    if 'nc_version' not in locals(): nc_version = 'NETCDF3_64BIT'
    # Run the main script
    createMonthlyForcing(zone,period,forcing_dir,era5_dir,nc_version)


