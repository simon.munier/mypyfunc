import os
import numpy as np
from netCDF4 import Dataset,num2date
from glob import glob
from pandas import date_range, Timedelta, DataFrame
from datetime import datetime
from .tqdm import tqdm,trange
from .misc import available_kwargs,NSE,KGE,ratio,correlation
from .map import get_IlonIlat

def surfex2trip_forcing(sfx_dir,trp_dir,output='surfex',run_dir=None):
  '''
  Convert SURFEX outputs to CTRIP forcings
  '''
  sfx_dir = sfx_dir.rstrip('/')
  trp_dir = trp_dir.rstrip('/')
  if output=='surfex':
    sfx_files = sorted(glob(sfx_dir+'/ISBA_DIAG_CUMUL*.nc'))
    cumulated = True
    if len(sfx_files) == 0:
      sfx_files = sorted(glob(sfx_dir+'/ISBA_DIAGNOSTICS*.nc'))
      cumulated = False
  elif output=='trip':
    sfx_files = sorted(glob(sfx_dir+'/TRIP_DIAG*.nc'))
  if run_dir is not None:
    run_dir = run_dir.rstrip('/')
    with Dataset(run_dir+'/data_oasis/rmp_slan_to_tlan_CONSERV_FRACNNEI.nc') as nc:
      Isrc = nc['src_address'][:]-1
      Idst = nc['dst_address'][:]-1
      rmp = nc['remap_matrix'][:]
    with Dataset(run_dir+'/pgd_prep/grids.nc') as nc:
      lon = nc['tlan.lon'][0,:]
      lat = nc['tlan.lat'][:,0]
    def isba2trip(R_isba):
      R = np.ma.MaskedArray(np.zeros((R_isba.shape[0],len(lon)*len(lat))),dtype='f4')
      for i in trange(rmp.shape[0]):
        R[:,Idst[i]] = R[:,Idst[i]]+R_isba[:,Isrc[i]]*rmp[i,0]
      R = R.reshape((-1,len(lat),len(lon)))
      return R
  for sfx_file in sfx_files:
    if output=='surfex':
      with Dataset(sfx_file) as nc:
        time_units = nc['time'].units
        time = nc['time'][:]
        if run_dir is None:
          lon = nc['lon'][:]
          lat = nc['lat'][:]
        if 'RUNOFFC_ISBA' in nc.variables:
          runoff = nc['RUNOFFC_ISBA'][:]
          drain = nc['DRAINC_ISBA'][:]
          print('Warning: Cumulated runoff and drainage used')
          print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
          print('!!!       PLEASE CHECK OPTION LCUMFRC       !!!')
          print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        else:
          #runoff = nc['RUNOFF_ISBA'][:]
          #drain = nc['DRAIN_ISBA'][:]
          runoff = np.ma.MaskedArray(np.zeros((len(time),len(lat),len(lon))),dtype='f4')
          drain = np.ma.MaskedArray(np.zeros((len(time),len(lat),len(lon))),dtype='f4')
          runoff[:] = nc['RUNOFF_ISBA'][:]*10800
          drain[:] = nc['DRAIN_ISBA'][:]*10800
          #runoff.mask[0,:,:] = runoff.mask[1,:,:]
          #drain.mask[0,:,:] = drain.mask[1,:,:]
          print('Warning: Instantaneous runoff and drainage used')
          print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
          print('!!!       PLEASE CHECK OPTION LCUMFRC       !!!')
          print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
      if run_dir is not None:
        runoff = isba2trip(runoff)
        drain = isba2trip(drain)
      trp_file = trp_dir+sfx_file[len(sfx_dir):].replace('ISBA_DIAG_CUMUL','TRIP_FORCING')
    elif output=='trip':
      with Dataset(sfx_file) as nc:
        time_units = nc['time'].units
        time = nc['time'][:]
        lon = nc['longitude'][:]
        lat = nc['latitude'][:]
        runoff = nc['RUNOFF'][:]
        drain = nc['DRAIN'][:]
      trp_file = sfx_file.replace(sfx_dir,trp_dir).replace('TRIP_DIAG','TRIP_FORCING')
    with Dataset(trp_file,'w') as nc:
      nc.createDimension('time',len(time))
      nc.createDimension('longitude',len(lon))
      nc.createDimension('latitude',len(lat))
      nc.createVariable('time','f4',('time',),fill_value=1e20)
      nc['time'].setncattr('units',time_units)
      nc['time'][:] = time-time[0]
      nc.createVariable('longitude','f4',('longitude',))
      nc['longitude'][:] = lon
      nc.createVariable('latitude','f4',('latitude',))
      nc['latitude'][:] = lat
      nc.createVariable('RUNOFF','f4',('time','latitude','longitude'),fill_value=1e20)
      nc['RUNOFF'][:] = runoff.filled(1e20)
      nc.createVariable('DRAIN','f4',('time','latitude','longitude'),fill_value=1e20)
      nc['DRAIN'][:] = drain.filled(1e20)
    print(trp_file)

def available_trip_files(res_dir,file_expr='TRIP_DIAG_*.nc'):
  '''
  Return a list of TRIP_DIAG files
  Add period from file name
  '''
  if isinstance(res_dir,list) or isinstance(res_dir,tuple) or isinstance(res_dir,np.ndarray):
    nc_files = [f for f in res_dir if os.path.isfile(f)]
  elif res_dir[-3:]=='.nc':
    nc_files = [res_dir]
  else:
    nc_files = glob(res_dir+'/'+file_expr)
  nc_files = np.sort(nc_files)
  return nc_files

def read_trip(res_dir,var='QDIS',file_expr='TRIP_DIAG*.nc',Ilon=None,Ilat=None,zone=None,
              period=None,dt=None,progressbar=True):
  '''
  Read TRIP outputs from results directory
  '''
  nc_files = available_trip_files(res_dir,file_expr=file_expr)
  if len(nc_files)==0:
    print('Warning: no TRIP_DIAG files in '+res_dir)
    return None,None,None,None
  # Read dimensions
  with Dataset(nc_files[0]) as nc:
    dat0 = num2date(nc['time'][:],nc['time'].units,only_use_cftime_datetimes=False)
    lon,lat = nc['longitude'][:],nc['latitude'][:]
    swap_time = True if nc[var].dimensions[2]=='time' else False
  # Define dates
  if dt is None:
    dt = dat0[1]-dat0[0] if len(dat0)>1 else Timedelta('1day')
  if period is None:
    dat_end = None
    for t_end in nc_files[-1].strip('.nc').split('_'):
      try:
        if   len(t_end)==4: dat_end = np.datetime64(t_end[:4])+1
        elif len(t_end)==6: dat_end = np.datetime64(t_end[:4]+'-'+t_end[4:6])+1
        elif len(t_end)==8: dat_end = np.datetime64(t_end[:4]+'-'+t_end[4:6]+'-'+t_end[6:8])+1
        else: continue
        break
      except:
        pass
    if dat_end is None: raise Exception('Error with date in file name')
    dat = date_range(dat0[0],dat_end,freq=dt)[:-1]
  else:
    period = [datetime.strptime(period[0],'%Y-%m-%d'),
              datetime.strptime(period[1],'%Y-%m-%d')+Timedelta('1day')]
    dat = date_range(period[0],period[1],freq=dt)[:-1]
    dat = dat+(dat0[0]-datetime(dat0[0].year,dat0[0].month,dat0[0].day))
  dat = dat.to_pydatetime()
  # Get slices or indices
  if zone is not None:
    Ilon,Ilat = get_IlonIlat(lon,lat,zone)
  slice_lon = True
  if Ilon is None: Ilon = slice(0,len(lon))
  if type(Ilon) in [list,tuple]: Ilon = np.array(Ilon)
  if isinstance(Ilon,slice): nlon = Ilon.stop-Ilon.start
  elif isinstance(Ilon,np.ndarray):
    if Ilon.dtype=='bool': nlon = Ilon.sum()
    else: nlon,slice_lon = len(Ilon),False
  else: Ilon,nlon,slice_lon = [Ilon],1,False
  slice_lat = True
  if Ilat is None: Ilat = slice(0,len(lat))
  if type(Ilat) in [list,tuple]: Ilat = np.array(Ilat)
  if isinstance(Ilat,slice): nlat = Ilat.stop-Ilat.start
  elif isinstance(Ilat,np.ndarray):
    if Ilat.dtype=='bool': nlat = Ilat.sum()
    else: nlat,slice_lat = len(Ilat),False
  else: Ilat,nlat,slice_lat = [Ilat],1,False
  if slice_lon!=slice_lat:
    raise Exception('Error in read_trip: only one of Ilon, Ilat is slice.')
  elif not slice_lon and nlon!=nlat:
    raise Exception('Error in read_trip: Ilon and Ilat must have the same dimension if list of indices.')
  lon,lat = lon[Ilon],lat[Ilat]
  # Read data
  if slice_lon:
    values = np.nan*np.zeros((len(dat),nlat,nlon),'f4')
  else:
    values = np.nan*np.zeros((len(dat),nlon),'f4')
  nc_files_list = tqdm(nc_files) if progressbar else nc_files
  eps = Timedelta('30sec')
  for nc_file in nc_files_list:
    with Dataset(nc_file) as nc:
      t = num2date(nc['time'][[0,-1]],nc['time'].units,only_use_cftime_datetimes=False)
    if t[0]>dat[-1] or t[-1]<dat[0]: continue
    nc = Dataset(nc_file)
    t = num2date(nc['time'][:],nc['time'].units,only_use_cftime_datetimes=False)
    Idat = slice(np.argmax(dat>=t[0]-eps) if any(dat>=t[0]-eps) else len(dat),
                 np.argmax(dat>t[-1]+eps) if any(dat>t[-1]+eps) else len(dat))
    ndat = Idat.stop-Idat.start
    It   = slice(np.argmax(t>=dat[0]-eps) if any(t>=dat[0]-eps) else len(t),
                 np.argmax(t>dat[-1]+eps) if any(t>dat[-1]+eps) else len(t))
    if slice_lon:
      if swap_time: values[Idat,:,:] = nc[var][Ilat,Ilon,It].transpose([2,0,1])
      else        : values[Idat,:,:] = nc[var][It,Ilat,Ilon]
    else:
      if False:
        for k in range(nlon):
          if swap_time: values[Idat,k] = nc[var][Ilat[k],Ilon[k],It]
          else        : values[Idat,k] = nc[var][It,Ilat[k],Ilon[k]]
      else:
        if swap_time:
          It_tmp = np.repeat(np.arange(It.start,It.stop).reshape((1,-1)),nlon,axis=0).T
          Ilon_tmp = np.repeat(np.array(Ilon).reshape((-1,1)),len(It_tmp),axis=1).T
          Ilat_tmp = np.repeat(np.array(Ilat).reshape((-1,1)),len(It_tmp),axis=1).T
          values[Idat,:] = nc[var][:][Ilat_tmp,Ilon_tmp,It_tmp]
        else:
          It_tmp = np.repeat(np.arange(It.start,It.stop).reshape((1,-1)),nlon,axis=0).T
          Ilon_tmp = np.repeat(np.array(Ilon).reshape((-1,1)),len(It_tmp),axis=1).T
          Ilat_tmp = np.repeat(np.array(Ilat).reshape((-1,1)),len(It_tmp),axis=1).T
          values[Idat,:] = nc[var][:][It_tmp,Ilat_tmp,Ilon_tmp]
    MV = 1e20
    if '_FillValue' in dir(nc[var]): MV = nc[var]._FillValue
    if 'missing_value' in dir(nc[var]): MV = nc[var].missing_value
    nc.close()
  values[values==MV] = np.nan
  dat = dat+dt/2
  return dat,lon,lat,values.squeeze()


def trip_r2d2(res_dir,period,zone=None,r2d2_file=None,ct=None,
              specific_discharge=False,stat_vars=['NSE'],
              **kwargs):
  '''
  Compute discharge statistics for a given experiment
  '''
  from R2D2 import Catalog,rdat
  if r2d2_file is None: r2d2_file = res_dir+'/CTRIP_R2D2.nc'
  # CTRIP files and zone
  kwargs_avail_trip = available_kwargs(available_trip_files,kwargs)
  nc_files = available_trip_files(res_dir,**kwargs_avail_trip)
  with Dataset(nc_files[0]) as nc:
    trip_lon,trip_lat = nc['longitude'][:],nc['latitude'][:]
  res = trip_lon[1]-trip_lon[0]
  res_str = '2D' if res==0.5 else '12D'
  if zone is None:
    zone = [[trip_lon.min()-res/2,trip_lat.max()+res/2],
            [trip_lon.max()+res/2,trip_lat.min()-res/2]]
  # R2D2 catalog
  if not os.path.isfile(r2d2_file):
    if ct is None:
      # Create stations catalog
      ct = Catalog(db='R2D2')
      kwargs_ct = available_kwargs(ct.select_stations,kwargs)
      if 'nb_values' in kwargs_ct: kwargs_ct.pop('nb_values')
      ct = ct.select_stations(zone=zone,**kwargs_ct)
      print('  * load trip data')
      ct.trip(res_str)
      print('  * select trip stations')
      for st in ct.st:
        st.trip_area = np.nan if st.trip_area=='--' else np.float(st.trip_area)
      Ist = ~np.isnan(ct.trip_area())
      ct = ct.select_stations(Ist=Ist)
      print('  * read data')
      ct.read_data(*period)
      if 'nb_values' in kwargs: ct = ct.select_stations(nb_values=kwargs['nb_values'])
    print('  * create nc file')
    ct.create_nc(r2d2_file,
                  trip_lon=('f4',{},ct.trip_lon()),
                  trip_lat=('f4',{},ct.trip_lat()),
                  trip_area=('f4',{},ct.trip_area()))
    print('  => done')
  else:
    # Read stations catalog
    print('  * read ctrip data')
    ct = Catalog(r2d2_file)
  Qobs = ct.Q
  #
  # CTRIP simulations
  if 'trip_data' not in ct.__dir__():
    Ilon = [np.argmin(np.abs(trip_lon-stx)) for stx in ct.trip_lon()]
    Ilat = [np.argmin(np.abs(trip_lat-sty)) for sty in ct.trip_lat()]
    kwargs_trip = available_kwargs(read_trip,kwargs)
    dat,lon,lat,Qsim = read_trip(res_dir,period=period,Ilon=Ilon,Ilat=Ilat,**kwargs_trip)
    #dat = dat.strftime('%Y-%m-%d')
    dat = rdat(dat,outfmt='str')
    ct.dat = rdat(ct.dat,outfmt='str')
    if not np.array_equal(dat,ct.dat):
      raise Exception('Inconsistency between dates from R2R2 catalog and CTRIP simulation.')
    with Dataset(r2d2_file,'a') as nc:
      nc.createVariable('trip_data','f4',('station','time'))
      nc['trip_data'].setncatts({'description':'Simulated discharge','units':'m3.s-1'})
      nc['trip_data'][:] = Qsim.T
  else:
    Qsim = ct.trip_data
  #
  # Specific discharge
  if specific_discharge:
    for i,(area_obs,area_sim) in enumerate(zip(ct.area(),ct.trip_area())):
      Qobs[:,i] = 1e-3*86400*Qobs[:,i]/area_obs
      Qsim[:,i] = 1e-3*86400*Qsim[:,i]/area_sim
  #
  stats = np.zeros((ct.Nst,len(stat_vars)),dtype='f4')
  for k,s in enumerate(stat_vars):
    if s == 'NSE':
      # Nash-Sutcliffe
      for i in range(ct.Nst):
        stats[i,k] = NSE(Qsim[:,i],Qobs[:,i])
    elif s == 'KGE':
      # Kling-Gupta Efficiency
      for i in range(ct.Nst):
        stats[i,k] = KGE(Qsim[:,i],Qobs[:,i])
    elif s == 'ratio':
      # Ratio
      for i in range(ct.Nst):
        stats[i,k] = ratio(Qsim[:,i],Qobs[:,i])
    elif s == 'corr':
      # Correlation
      for i in range(ct.Nst):
        stats[i,k] = correlation(Qsim[:,i],Qobs[:,i])
  #
  # Save statistics
  with Dataset(r2d2_file,'a') as nc:
    for k,s in enumerate(stat_vars):
      if s not in nc.variables:
        nc.createVariable(s,'f4',('station'))
      nc[s][:] = stats[:,k]


def read_modcou(res_dir,file_expr='Debits_modcou*',Ist=None,period=None,freq='3H',
                daily_output=True,progressbar=True):
  '''
  Read MODCOU outputs from results directory
  '''
  Nst = 1163
  modcou_files = available_trip_files(res_dir,file_expr=file_expr)
  if len(modcou_files)==0:
    print('Warning: no MODCOU output files in '+res_dir)
    return None,None,None,None
  if period is None:
    period = [modcou_files[0][-9:-5],modcou_files[-1][-4:]]
  period = [period[0]+'-08-01 06:00:00',period[1]+'-08-01 06:00:00']
  dat = date_range(*period,freq=freq)[:-1].to_pydatetime()
  if Ist is None: Ist = np.arange(Nst)
  Qmodcou = np.zeros((len(dat),len(Ist)),'f4')
  for f in tqdm(modcou_files):
    Idat = (dat>=datetime(int(f[-9:-5]),8,1,6))*(dat<datetime(int(f[-4:]),8,1,6))
    if Idat.sum()==0: continue
    l = open(f).readlines()
    for i,j in enumerate(np.where(Idat)[0]):
      v = ''.join(l[i*117:(i+1)*117]).replace('\n','').split()
      for k in range(len(v)):
        if v[k][-4]!='E': v[k] = 0
      Qmodcou[j,:] = [float(v[k]) for k in Ist]
  if daily_output:
    Dsub = DataFrame(index=dat,data=Qmodcou)
    #Dday = Dsub.resample('1D',loffset='12H').mean()
    Dday = Dsub.resample('1D').mean()
    dat = Dday.index.to_pydatetime()
    Qmodcou = Dday.values
  return dat,Qmodcou




